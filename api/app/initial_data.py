"""Module for creating initial data for the database"""
#!/usr/bin/env python3

from datetime import date
from sqlalchemy.orm import Session
from app.database import models
from app.database.models import Status
from app.database.session import SessionLocal


def truncate_tables() -> None:
    """Truncate tables"""

    database: Session = SessionLocal()

    database.execute('truncate table "form" cascade')

    database.commit()


def create_initial_data() -> None:
    """Creates initial data for database"""
    database: Session = SessionLocal()

    form1 = models.Form(
        id="c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        name="formulier a",
        slug="formulier-a",
        url="http://formulieren.openformulieren.io/forms/"
        + "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        status=Status.INACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=date(2001, 1, 1),
    )
    form2 = models.Form(
        id="69c08406-3984-4344-9b6d-6f4adec2a4b7",
        name="formulier b",
        slug="formulier-b",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b7",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=date(2001, 1, 1),
    )
    css1 = models.Css(
        id="69c08406-3984-4344-9b6d-6f4adec2a4b7",
        name="css1",
        primary_color = "Blue",
        secondary_color= "Blue",
        font = "Blue",
        color_left = "Blue",
        background_color= "Blue",
        logo = "Blue",
    )
    css2 = models.Css(
        id="69c08406-3984-4344-9b6d-6f4adec2a4b6",
        name="css2",
        primary_color = "Blue",
        secondary_color= "Blue",
        font = "Blue",
        color_left = "Blue",
        background_color= "Blue",
        logo = "Blue",
    )

    database.add(form1)
    database.add(form2)
    database.add(css1)
    database.add(css2)
    database.flush()
    database.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
