"""Module for form CRUD calls"""

from app import schemas
from app.database import models
from app.domain.base import CRUDBase


class CRUDForm(CRUDBase[models.Form, schemas.FormCreate, schemas.FormUpdate]):
    """Class for form CRUD calls"""
    pass


form = CRUDForm(models.Form)
