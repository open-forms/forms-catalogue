"""Base Class table"""

import uuid
from sqlalchemy.ext.declarative import as_declarative, declared_attr


@as_declarative()
class Base:
    """The Base class"""
    id: uuid.UUID
    slug: str
    __name__: str

    # Generate __tablename__ automatically

    @declared_attr
    def __tablename__(self, cls) -> str:
        """Table name"""
        return cls.__name__.lower()
