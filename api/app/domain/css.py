"""Module for css CRUD calls"""

from app import schemas
from app.database import models
from app.domain.base import CRUDBase


class CRUDCss(CRUDBase[models.Css, schemas.CssCreate, schemas.CssUpdate]):
    """Class for css CRUD calls"""
    pass


css = CRUDCss(models.Css)
