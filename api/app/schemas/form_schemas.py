"""Module for the form schema"""

from __future__ import annotations

import uuid as py_uuid
from datetime import date
from typing import Optional
from pydantic import BaseModel, Field, constr, validator
from app.core.types import SortOrder
from app.database.models import Status
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional

max_length = constr(max_length=255)


class FormBase(BaseModel):
    """Form base model"""
    name: max_length = Field(..., title="The name of the form")
    status: Status = Field(..., title="The status of the form")
    form: str = Field(..., title="The form of the form")

    class Config:
        """Config for FormBase"""
        allow_population_by_field_name = True


class FormCreateMinimal(FormBase):
    """Form used to validate minimal input"""
    # slug: str = Field(..., title="The slug of the form")
    # url: str = Field(..., title="The url of the form")
    config: Optional[str] = Field(None, title="The config of the form")
    organization_id: Optional[py_uuid.UUID] = Field(
        None, title="The organization id of the form", alias="organizationId"
    )
    organization_url: Optional[str] = Field(
        None,
        title="The organization url of the form",
        alias="organizationUrl",
    )
    publish_date: Optional[date] = Field(
        None, title="The publish date of the form", alias="publishDate"
    )

    @validator("publish_date", pre=True, always=False)
    def validate_publish_date(cls, val):
        """Validates the publish date"""
        if val == "":
            return None
        return val


class FormCreate(FormCreateMinimal):
    """Form used to validate input for create api call, after user input, with newly generated slug"""
    slug: str = Field(..., title="The slug of the form")
    url: str = Field(..., title="The url of the form")


class Form(FormCreate):
    """Form used for response body"""
    id: py_uuid.UUID = Field(..., title="The unique uuid of the form")

    class Config:
        """Config for Form"""
        orm_mode = True
        extra = "forbid"


class FormUpdate(to_optional(FormCreate)):
    """Make fields optional to support partial update (patch)"""
    pass


FormUpdate.update_forward_refs()


class FormFilterableFields(FilterableId):
    """Class for the fields that are filterable"""
    status: Optional[str]
    q: Optional[str]

    class Config:
        """Config for FormFilterableFields"""
        extra = "forbid"


class FormSortableFields(BaseModel):
    """Class for the fields that are sortable"""
    id: Optional[SortOrder]
    name: Optional[SortOrder]
    status: Optional[SortOrder]
    publishDate: Optional[SortOrder] = Field(alias="publish_date")
    slug: Optional[SortOrder]

    class Config:
        """Config for FormSortableFields"""
        allow_population_by_field_name = True
        extra = "forbid"
