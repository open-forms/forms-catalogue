"""Module for the user schema"""

from __future__ import annotations

from pydantic import BaseModel, Field


class UserResponse(BaseModel):
    """UserResponse schema"""
    username: str = Field(..., title="The username of the user")


class User(UserResponse):
    """User schema"""
    password: str = Field(..., title="The password of the user")
