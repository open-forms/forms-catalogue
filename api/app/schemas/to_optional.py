"""Module to set the fields of a BaseModel to optional for doing an update api call"""

from copy import deepcopy
from typing import Optional, Type, TypeVar
from pydantic import BaseModel, create_model

BaseModelT = TypeVar("BaseModelT", bound=BaseModel)


def to_optional(
    model: Type[BaseModelT], model_name: Optional[str] = None
) -> Type[BaseModelT]:
    """
    Create a new BaseModel with the exact same fields as `model`
    but making them all optional
    """
    all_annotations = {}
    # Workaround to retrieve the passed type and not the resolved one
    # (e.g. `str` with constraints that resolved in `ConstrainedStrValue`)
    for base in reversed(model.__bases__):
        all_annotations.update(getattr(base, "__annotations__", {}))
    all_annotations.update(model.__annotations__)

    field_definitions = {}

    for field_name, field in model.__fields__.items():
        optional_field_info = deepcopy(field.field_info)
        # Do not change default value of fields that are already optional
        if optional_field_info.default is ...:
            optional_field_info.default = None

        field_type = all_annotations.get(field_name, field.outer_type_)
        field_definitions[field_name] = (field_type, optional_field_info)

    return create_model(
        model_name or f"Optional{model.__name__}",
        __base__=model,
        **field_definitions,  # type: ignore[arg-type]
    )
