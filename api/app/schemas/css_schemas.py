"""Module for the css schema"""

from __future__ import annotations

import uuid as py_uuid
from typing import Optional
from pydantic import BaseModel, Field, constr
from app.core.types import SortOrder
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional

max_length = constr(max_length=255)


class CssBase(BaseModel):
    """CSS base model"""
    name: max_length = Field(..., title="The name of the CSS")
    primary_color: str = Field(
        ..., title="The primary color of the CSS", alias="primaryColor"
    )
    secondary_color: str = Field(
        ..., title="The secondary color of the CSS", alias="secondaryColor"
    )
    font: str = Field(..., title="The font of the CSS")
    color_left: str = Field(
        ..., title="The color on the left of the CSS", alias="colorLeft"
    )
    background_color: str = Field(
        ..., title="The background color of the CSS", alias="backgroundColor"
    )
    logo: str = Field(..., title="The logo of the CSS")

    class Config:
        allow_population_by_field_name = True


class CssCreate(CssBase):
    """Css used to validate input for create api call"""
    pass


class Css(CssBase):
    """Properties to return to client"""
    id: py_uuid.UUID = Field(..., title="The unique uuid of the CSS")

    class Config:
        """Config for Css"""
        orm_mode = True
        extra = "forbid"


class CssUpdate(to_optional(CssCreate)):
    """Make fields optional to support partial update (patch)"""
    pass


CssUpdate.update_forward_refs()


class CssFilterableFields(FilterableId):
    """Class for the fields that are filterable"""
    name: Optional[str]
    q: Optional[str]

    class Config:
        """Config for CssFilterableFields"""
        extra = "forbid"


class CssSortableFields(BaseModel):
    """Class for the fields that are sortable"""
    name: Optional[SortOrder]

    class Config:
        """Config for CssSortableFields"""
        extra = "forbid"
