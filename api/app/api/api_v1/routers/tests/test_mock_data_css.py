#!/usr/bin/env python3

from app.database import models
from collections import namedtuple
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "css1",
        "css2",
    ],
)


#  Same data als initial_data.
def add_mock_data_to_db(db: Session) -> MockData:

    css1 = models.Css(
        id=None,
        name="css1",
        primary_color="red",
        secondary_color="blue",
        font="ariel",
        color_left="red",
        background_color="white",
        logo="asfre=",
    )
    css2 = models.Css(
        id=None,
        name="css2",
        primary_color="red",
        secondary_color="yellow",
        font="ariel",
        color_left="yellow",
        background_color="black",
        logo="asfre=",
    )

    db.add(css1)
    db.add(css2)

    db.flush()

    return MockData(
        css1,
        css2,
    )
