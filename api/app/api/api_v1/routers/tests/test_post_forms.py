import pytest
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all the required fields filled
# ---------------------------------------------
def test_post_forms_all_required_fields_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_all_required_fields_set",
        "status": mock.statusActive,
        "form": "{}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all fields filled
# Id will be ignored.
# Slug will be ignored.
# Url will be ignored.
# ---------------------------------------------
def test_post_forms_all_fields_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_all_fields_set",
        "slug": "test_post_forms_all_fields_set slug",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "{}",
        "config": "{}",
        "organizationId": "69c08406-3984-4344-9b6d-6f4adec2a4b5",
        "organizationUrl": "http://localhost3",
        "publishDate": "2001-01-02",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["id"] != body["id"]
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["slug"] == body["name"].replace("_", "-")
    assert res["url"] is not None
    assert res["url"] != body["url"]
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] == body["config"]
    assert res["organizationId"] == body["organizationId"]
    assert res["organizationUrl"] == body["organizationUrl"]
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with empty body.
# ---------------------------------------------
def test_post_forms_body_is_empty(
    client: TestClient,
):
    body = {}
    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 3
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "status"
    assert res["detail"][1]["msg"] == "field required"
    assert res["detail"][1]["type"] == "value_error.missing"
    assert res["detail"][2]["loc"]["source"] == "body"
    assert res["detail"][2]["loc"]["field"] == "form"
    assert res["detail"][2]["msg"] == "field required"
    assert res["detail"][2]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all form values empty.
# ---------------------------------------------
def test_post_forms_all_fields_values_are_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "",
        "name": "",
        "slug": "",
        "url": "",
        "status": "",
        "form": "",
        "config": "",
        "organizationId": "",
        "organizationUrl": "",
        "publishDate": "",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 2
    assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
    assert res["detail"][0]["ctx"]["enum_values"][0] == mock.statusActive
    assert res["detail"][0]["ctx"]["enum_values"][1] == mock.statusInactive
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert (
        res["detail"][0]["msg"]
        == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
    )
    assert res["detail"][0]["type"] == "type_error.enum"

    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "organizationId"
    assert res["detail"][1]["msg"] == "value is not a valid uuid"
    assert res["detail"][1]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled id.
# Id will be ignored.
# ---------------------------------------------
@pytest.mark.parametrize(
    "id_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        None,
    ],
)
def test_post_forms_with_id(
    client: TestClient, test_db: Session, id_value: str
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": f"{id_value}",
        "name": "test_post_forms_with_id",
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["id"] != body["id"]
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] == body["name"].replace("_", "-")
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field name
# ---------------------------------------------
def test_post_forms_name_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with empty field name
# TODO: cannot add a form with an empty field name.
# ---------------------------------------------
def test_post_forms_name_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "",
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201  # TODO 422
    # res = response.json()
    # assert len(res["detail"]) == 1
    # assert res["detail"][0]["loc"]["source"] == "body"
    # assert res["detail"][0]["loc"]["field"] == "name"
    # assert res["detail"][0]["msg"] == "field required"
    # assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with name is none
# ---------------------------------------------
def test_post_forms_name_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": None,
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "none is not an allowed value"
    assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with name that exceeds 255 kars.
# ---------------------------------------------
def test_post_forms_name_value_exceeds_max_length(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_name_value_exceeds_max_length 72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "status": mock.statusActive,
        "form": "{}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert (
        res["detail"][0]["msg"]
        == "ensure this value has at most 255 characters"
    )  # noqa: E501
    assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with multiple names.
# ---------------------------------------------
def test_post_forms_multiple_names(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_multiple_names 1",  # noqa: F601
        "name": "test_post_forms_multiple_names 2",  # noqa: F601
        "name": "test_post_forms_multiple_names 3",  # noqa: F601
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }
    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] is not None
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new active form where form name already exists.
# ---------------------------------------------
def test_post_forms_name_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form1.name,
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 409
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Conflict"
    assert (
        res["detail"][0]["message"]
        == "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam."  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_unique"


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled slug.
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_with_slug(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_slug",
        "status": mock.statusActive,
        "form": "{}",
        "slug": "formn6",
        "publishDate": None,
    }
    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] != body["slug"]
    assert res["slug"] == body["name"].replace("_", "-")
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form without optional field slug.
# ---------------------------------------------
def test_post_forms_slug_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_not_set",
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with empty field slug
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_is_empty",
        "slug": "",
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with slug is none
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_is_none",
        "slug": None,
        "form": "{}",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with a slug that exceeds 255 kars.
# Slug will be ignored.
# TODO: check max length
# ---------------------------------------------
def test_post_forms_slug_exceeds_max_length(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_exceeds_max_length",
        "status": mock.statusActive,
        "form": "{}",
        "slug": "formulierrneww42345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with multiple slugs.
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_multiple_slugs(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_multiple_slugs",
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form where slug already exists
# Slug will be ignored.
# ---------------------------------------------
def test_post_forms_slug_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_slug_exists",
        "slug": mock.form6.slug,
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled url.
# Url will be ignored.
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_post_forms_with_url(
    client: TestClient,
    test_db: Session,
    url_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_url",
        "status": mock.statusActive,
        "form": "{}",
        "url": f"{url_value}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["url"] != body["url"]
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with status inactive.
# ---------------------------------------------
def test_post_forms_status_inactive(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_inactive",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["form"] is not None
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    for field in body:
        assert res[field] == body[field]


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with status active.
# ---------------------------------------------
def test_post_forms_status_active(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_active",
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    for field in body:
        assert res[field] == body[field]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is set to application/json.
# Test add a new form with status active.
# ---------------------------------------------
def test_post_forms_status_active_with_content_type_json(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": "application/json",
    }

    body = {
        "name": "test_post_forms_status_active_with_content_type_json",
        "status": mock.statusActive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
        headers=headers,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    for field in body:
        assert res[field] == body[field]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field status
# ---------------------------------------------
def test_post_forms_status_not_set(
    client: TestClient,
):

    body = {
        "name": "test_post_forms_status_not_set",
        "form": "{}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with invalid status value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "status_invalid_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "INVALID_ACTIVE",
        "active",
        "inactive",
        None,
    ],
)
def test_post_forms_status_invalid_value(
    client: TestClient,
    test_db: Session,
    status_invalid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_status_invalid_value",
        "status": f"{status_invalid_value}",
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
    assert res["detail"][0]["ctx"]["enum_values"][0] == mock.statusActive
    assert res["detail"][0]["ctx"]["enum_values"][1] == mock.statusInactive
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["loc"]["source"] == "body"
    assert (
        res["detail"][0]["msg"]
        == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
    )
    assert res["detail"][0]["type"] == "type_error.enum"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an empty form.
# ---------------------------------------------
def test_post_forms_form_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_form_empty",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "",
        "config": "{}",
        "organizationUrl": "http://localhost2",
        "publishDate": "2001-01-01",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["url"] != body["url"]
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] == body["config"]
    assert res["organizationId"] is None
    assert res["organizationUrl"] == body["organizationUrl"]
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required field form
# ---------------------------------------------
def test_post_forms_form_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_form_not_set",
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "form"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an invalid form.
# TODO: form always json?
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_form",
    [
        # "",
        # " ",
        # "123",
        None,
    ],
)
def test_post_forms_form_not_valid(
    client: TestClient,
    test_db: Session,
    invalid_form: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_form_not_valid",
        "slug": "test_post_forms_form_not_valid slug",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": invalid_form,
        "config": "{}",
        "organizationId": "69c08406-3984-4344-9b6d-6f4adec2a4b5",
        "organizationUrl": "http://localhost2",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "form"
    assert res["detail"][0]["msg"] == "none is not an allowed value"
    assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields form and status
# ---------------------------------------------
def test_post_forms_status_and_form_not_set(
    client: TestClient,
):
    body = {
        "name": "test_post_forms_status_and_form_not_set",
    }
    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "status"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "form"
    assert res["detail"][1]["msg"] == "field required"
    assert res["detail"][1]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields name and form
# ---------------------------------------------
def test_post_forms_name_and_form_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "form"
    assert res["detail"][1]["msg"] == "field required"
    assert res["detail"][1]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with missing required fields name and status
# ---------------------------------------------
def test_post_forms_name_and_status_not_set(
    client: TestClient,
):
    body = {
        "form": "{}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 2
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "status"
    assert res["detail"][1]["msg"] == "field required"
    assert res["detail"][1]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form without the required fields and with
# additional fields.
# ---------------------------------------------
def test_post_forms_without_required_fields_and_with_additional_fields(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "notvalid1": "formulier new 2",
        "notvalid2": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 3
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "name"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"
    assert res["detail"][1]["loc"]["source"] == "body"
    assert res["detail"][1]["loc"]["field"] == "status"
    assert res["detail"][1]["msg"] == "field required"
    assert res["detail"][1]["type"] == "value_error.missing"
    assert res["detail"][2]["loc"]["source"] == "body"
    assert res["detail"][2]["loc"]["field"] == "form"
    assert res["detail"][2]["msg"] == "field required"
    assert res["detail"][2]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add form with the required fields and with
# additional fields.
# ---------------------------------------------
def test_post_forms_with_required_fields_and_with_additional_fields(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_with_required_fields_and_with_additional_fields",  # noqa: E501
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "",
        "config": "{}",
        "organizationUrl": "http://localhost2",
        "publishDate": "2001-01-01",
        "notvalid1": "formulier new 2",
        "notvalid2": mock.statusActive,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] == body["name"].replace("_", "-")
    assert res["url"] is not None
    assert res["url"] != body["url"]
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] == body["config"]
    assert res["organizationId"] is None
    assert res["organizationUrl"] == body["organizationUrl"]
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an invalid organization id.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_organization_id",
    [
        "",
        " ",
        "123",
    ],
)
def test_post_forms_organization_id_not_valid(
    client: TestClient,
    test_db: Session,
    invalid_organization_id: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_id_not_valid",
        "slug": "test_post_forms_organization_id_not_valid slug",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "{}",
        "config": "{}",
        "organizationId": f"{invalid_organization_id}",
        "organizationUrl": "http://localhost2",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )
    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "organizationId"
    assert res["detail"][0]["msg"] == "value is not a valid uuid"
    assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with an none organization id.
# TODO 201 -> 422
# ---------------------------------------------
def test_post_forms_organization_id_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_id_none",
        "slug": "test_post_forms_organization_id_none slug",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "{}",
        "config": "{}",
        "organizationId": None,
        "organizationUrl": "http://localhost2",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )
    assert response.status_code == 201  # 422
    # res = response.json()
    # assert len(res) == 1
    # assert res["detail"][0]["loc"]["source"] == "body"
    # assert res["detail"][0]["loc"]["field"] == "organizationId"
    # assert res["detail"][0]["msg"] == "value is not a valid uuid"
    # assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with a valid organization id.
# ---------------------------------------------
@pytest.mark.parametrize(
    "valid_organization_id",
    [
        "12345678-1234-4123-a123-1234567890ab",
        "00000000-0000-4000-a000-000000000000",
        "aaaaaaaa-aaaa-4aaa-aaaa-aaaaaaaaaaaa",
    ],
)
def test_post_forms_organization_id_valid(
    client: TestClient,
    test_db: Session,
    valid_organization_id: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "1234567",
        "name": "test_post_forms_organization_id_valid",
        "slug": "test_post_forms_organization_id_valid slug",
        "url": "http://localhost",
        "status": mock.statusActive,
        "form": "{}",
        "config": "{}",
        "organizationId": f"{valid_organization_id}",
        "organizationUrl": "http://localhost2",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )
    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["id"] != body["id"]
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["url"] != body["url"]
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] == body["config"]
    assert res["organizationId"] == body["organizationId"]
    assert res["organizationUrl"] == body["organizationUrl"]
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled config.
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_post_forms_config(
    client: TestClient,
    test_db: Session,
    config_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_config",
        "status": mock.statusActive,
        "form": "{}",
        "config": f"{config_value}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] == body["config"]
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a prefilled organizationUrl.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_post_forms_organization_url(
    client: TestClient,
    test_db: Session,
    organization_url_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_organization_url",
        "status": mock.statusInactive,
        "form": "{}",
        "organizationUrl": f"{organization_url_value}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] == body["organizationUrl"]
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with an empty publishDate.
# ---------------------------------------------
def test_post_forms_publish_date_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_is_empty",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": "",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with publishDate set to None.
# ---------------------------------------------
def test_post_forms_publish_date_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_is_none",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with an invalid publishDate.
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_invalid_value",
    [
        "..;/",
        "0000-00-00",
        "0000-00-01",
        "0000-01-00",
        "0000-01-01",
        "01-01-2020",
        "2021-01-00",
        "2021-00-01",
        "0000-00-00",
        "2021-000-01",
        "2021-01-00",
        "2021-13-01",
        "2021-00-00",
        "2021-01-32",
        "2021-01",
        "2021-01-",
        "   ",
        "http://localhost",
    ],
)
def test_post_forms_publish_date_invalid_value(
    client: TestClient,
    test_db: Session,
    publish_date_invalid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_invalid_value",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": f"{publish_date_invalid_value}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"]["source"] == "body"
    assert res["detail"][0]["loc"]["field"] == "publishDate"
    assert res["detail"][0]["msg"] == "invalid date format"
    assert res["detail"][0]["type"] == "value_error.date"


# ---------------------------------------------
# Testing an API with POST requests
# No request header content type is set.
# Test add a new form with a valid publishDate.
# TODO: check date format
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_valid_value",
    [
        "0001-01-01",
        "2001-01-01",
        "2021-12-31",
        # "0", -> 1970-01-01
        # "2021", -> 1970-01-01
        # "1233344", -> 1970-01-15
        # "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456", -> 9999-12-31 # noqa: E501
    ],
)
def test_post_forms_publish_date_valid_value(
    client: TestClient,
    test_db: Session,
    publish_date_valid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_publish_date_valid_value",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": f"{publish_date_valid_value}",
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with POST requests
# Request header content type is not set.
# Test add a new form with all the required fields filled
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "",
        "1",
        "2",
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
        "../form",
        None,
    ],
)
def test_post_forms_all_required_fields_set_with_query_param(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_post_forms_all_required_fields_set_with_query_param",
        "status": mock.statusInactive,
        "form": "{}",
    }

    response = client.post(
        f"{prefix}{forms}?{query_parameter}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] == body["name"]
    assert res["slug"] is not None
    assert res["url"] is not None
    assert res["status"] == body["status"]
    assert res["form"] == body["form"]
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None
