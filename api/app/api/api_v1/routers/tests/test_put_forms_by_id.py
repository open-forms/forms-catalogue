import pytest
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from pydantic import ValidationError
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update all fields.
# Ignore the id.
# ---------------------------------------------
def test_put_forms_update_all_fields(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    mock_form_id = mock.form1.id
    mock_form_slug = "test-put-forms-update-all-fields-a"

    body = {
        "id": "c16ae581-8d6f-424e-bff3-3a70a1374f00",
        "name": "test_put_forms_update_all_fields a",
        "slug": mock_form_slug,
        "url": "http://formulieren.openformulieren.io/forms/c16ae581-8d6f-424e-bff3-3a70a1374f00",  # noqa: E501
        "status": mock.statusActive,
        "form": "{put}",
        "config": "put_config",
        "organizationId": "c16ae581-8d6f-424e-bff3-3a70a1374f09",
        "organizationUrl": "put_org_url",
        "publishDate": "2021-12-31",
    }

    for id in [
        mock_form_id,
        mock_form_slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] != body["id"]
        assert res["id"] == str(mock_form_id)
        assert res["name"] == body["name"]
        assert res["slug"] == body["slug"]
        assert res["url"] == body["url"]
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] == body["config"]
        assert res["organizationId"] == body["organizationId"]
        assert res["organizationUrl"] == body["organizationUrl"]
        assert res["publishDate"] == body["publishDate"]


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update with body is empty.
# Update nothing.
# ---------------------------------------------
def test_put_forms_body_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {}

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == mock.form1.name
        assert res["slug"] == mock.form1.slug
        assert res["url"] == mock.form1.url
        assert res["status"] == mock.form1.status
        assert res["form"] == mock.form1.form
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test update form with all form values empty.
# ---------------------------------------------
def test_put_forms_update_all_fields_values_are_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": "",
        "name": "",
        "slug": "",
        "url": "",
        "status": "",
        "form": "",
        "config": "",
        "organizationId": "",
        "organizationUrl": "",
        "publishDate": "",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 422
        res = response.json()
        assert len(res["detail"]) == 2
        assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "status"
        assert (
            res["detail"][0]["msg"]
            == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
        )
        assert res["detail"][0]["type"] == "type_error.enum"
        assert res["detail"][0]["ctx"]["enum_values"][0] == mock.statusActive
        assert res["detail"][0]["ctx"]["enum_values"][1] == mock.statusInactive

        assert res["detail"][1]["loc"]["source"] == "body"
        assert res["detail"][1]["loc"]["field"] == "organizationId"
        assert res["detail"][1]["msg"] == "value is not a valid uuid"
        assert res["detail"][1]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Update status and form.
# ---------------------------------------------
def test_put_forms_update_status_and_form(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:

        body = {
            "status": mock.statusInactive,
            "form": "{test1}",
        }

        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == mock.form1.name
        assert res["slug"] == mock.form1.slug
        assert res["url"] == mock.form1.url
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test without path parameter id and without a body.
# ---------------------------------------------
def test_put_forms_without_path_parameter_id_without_body(
    client: TestClient,
):

    response = client.put(
        f"{prefix}{forms}",
    )
    assert response.status_code == 405  # Method not allowed
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test without path parameter id and with an empty body.
# ---------------------------------------------
def test_put_forms_without_path_parameter_id_with_empty_body(
    client: TestClient,
):
    body = {}
    response = client.put(
        f"{prefix}{forms}/",
        json=body,
    )
    assert response.status_code == 307  # Temporary Redirect
    headers = response.headers
    assert len(headers) == 1
    assert headers["Location"] is not None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with empty path parameter id and without a body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id",
    [
        "",
    ],
)
def test_put_forms_with_empty_path_parameter_id_without_body(
    client: TestClient,
    invalid_id: str,
):
    response = client.put(
        f"{prefix}{forms}/{invalid_id}",
    )
    assert response.status_code == 307  # Temporary Redirect
    headers = response.headers
    assert len(headers) == 1
    assert headers["Location"] is not None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and without a body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_with_invalid_path_parameter_id_without_body(
    client: TestClient,
    invalid_path_parameter_id_value: str,
):
    response = client.put(
        f"{prefix}{forms}/{invalid_path_parameter_id_value}",
    )
    assert response.status_code == 422
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["loc"][0] == "body"
    assert res["detail"][0]["msg"] == "field required"
    assert res["detail"][0]["type"] == "value_error.missing"

    headers = response.headers
    assert len(headers) == 2
    assert headers["content-length"] is not None
    assert headers["content-type"] == "application/json"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and with an empty body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_invalid_id_with_empty_body(
    client: TestClient,
    invalid_path_parameter_id_value: str,
):
    body = {}

    response = client.put(
        f"{prefix}{forms}/{invalid_path_parameter_id_value}",
        json=body,
    )
    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {invalid_path_parameter_id_value}"  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Test with invalid path parameter id and with an empty body.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_put_forms_invalid_id_with_empty_body_2(
    client: TestClient,
    test_db: Session,
    invalid_path_parameter_id_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {}

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}{invalid_path_parameter_id_value}",
        json=body,
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form1.id}{invalid_path_parameter_id_value}"  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled id.
# Id will be ignored.
# ---------------------------------------------
@pytest.mark.parametrize(
    "id_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        None,
    ],
)
def test_put_forms_update_id(
    client: TestClient,
    test_db: Session,
    id_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "id": f"{id_value}",
        "form": "{}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["id"] != body["id"]
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == mock.form1.name
        assert res["slug"] == mock.form1.slug
        assert res["url"] == mock.form1.url
        assert res["status"] == mock.form1.status
        assert res["form"] == body["form"]
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty name
# TODO: cannot update a form with an empty name.
# ---------------------------------------------
def test_put_forms_name_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # TODO 422
        res = response.json()
        assert res["name"] is not None
        assert res["name"] == body["name"]

        # assert len(res["detail"]) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "name"
        # assert res["detail"][0]["msg"] == "field required"
        # assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with name is none
# TODO: catch validation error
# ---------------------------------------------
def test_put_forms_name_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": None,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        # pydantic.error_wrappers.ValidationError: 1 validation error for Form # noqa: E501
        # response -> name
        # none is not an allowed value (type=type_error.none.not_allowed)
        with pytest.raises(ValidationError):
            client.put(
                f"{prefix}{forms}/{id}",
                json=body,
            )

        # assert response.status_code == 422
        # res = response.json()
        # assert len(res["detail"]) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "name"
        # assert res["detail"][0]["msg"] == "none is not an allowed value"
        # assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test put form with name that exceeds 255 kars.
# ---------------------------------------------
def test_put_forms_name_value_exceeds_max_length(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "formulier update 72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 422
        res = response.json()
        assert len(res) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "name"
        assert (
            res["detail"][0]["msg"]
            == "ensure this value has at most 255 characters"
        )
        assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with multiple names.
# ---------------------------------------------
def test_put_forms_multiple_names(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "formulier update 9a",  # noqa: F601
        "name": "formulier update 9b",  # noqa: F601
        "name": "formulier update 9c",  # noqa: F601
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["name"] is not None
        assert res["name"] == mock.form1.name


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update form name.
# ---------------------------------------------
def test_put_forms_update_name(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": f"{mock.form1.name}a",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == body["name"]
        assert res["slug"] == mock.form1.slug
        assert res["url"] == mock.form1.url
        assert res["status"] == mock.form1.status
        assert res["form"] == mock.form1.form
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where form name already exists.
# ---------------------------------------------
def test_put_forms_by_id_name_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form2.name,
    }

    for id in [
        mock.form1.id,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 409
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["name"] == "Conflict"
        assert (
            res["detail"][0]["message"]
            == "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam."  # noqa: E501
        )
        assert res["detail"][0]["stack"]["type"] == "semantic_error.not_unique"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form where form name already exists.
# ---------------------------------------------
def test_put_forms_by_slug_name_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": mock.form2.name,
    }

    for id in [
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 409
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["name"] == "Conflict"
        assert (
            res["detail"][0]["message"]
            == "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam."  # noqa: E501
        )
        assert res["detail"][0]["stack"]["type"] == "semantic_error.not_unique"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update slug.
# ---------------------------------------------
def test_put_forms_slug(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    mock_form1_id = mock.form1.id
    mock_form1_slug = "test-put-forms-slug"

    body = {
        "slug": f"{mock_form1_slug}",
    }

    for id in [
        mock_form1_id,
        mock_form1_slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == mock.form1.name
        assert res["slug"] == body["slug"]
        assert res["url"] == mock.form1.url
        assert res["status"] == mock.form1.status
        assert res["form"] == mock.form1.form
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form without optional field slug.
# ---------------------------------------------
def test_put_forms_slug_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "form": "{}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] is not None
        assert res["form"] == body["form"]
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty slug
# TODO: cannot update a form with an empty slug.
# ---------------------------------------------
def test_put_forms_slug_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        # mock.form1.slug,  TODO implement this.
    ]:
        body = {
            "slug": "",
        }

        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # TODO 422
        # res = response.json()
        # assert len(res["detail"]) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "name"
        # assert res["detail"][0]["msg"] == "field required"
        # assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with slug is none
# Slug will be ignored.
# TODO: catch validation error
# ---------------------------------------------
def test_put_forms_by_id_slug_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": None,
    }

    # pydantic.error_wrappers.ValidationError: 1 validation error for Form # noqa: E501
    # response -> slug
    # none is not an allowed value (type=type_error.none.not_allowed)
    with pytest.raises(ValidationError):
        client.put(
            f"{prefix}{forms}/{mock.form1.id}",
            json=body,
        )

    # assert response.status_code == 422
    # res = response.json()
    # assert len(res["detail"]) == 1
    # assert res["detail"][0]["loc"]["source"] == "body"
    # assert res["detail"][0]["loc"]["field"] == "slug"
    # assert res["detail"][0]["msg"] == "none is not an allowed value"
    # assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with slug is none
# Slug will be ignored.
# TODO: catch validation error
# ---------------------------------------------
def test_put_forms_by_slug_slug_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": None,
    }

    # pydantic.error_wrappers.ValidationError: 1 validation error for Form # noqa: E501
    # response -> slug
    # none is not an allowed value (type=type_error.none.not_allowed)
    with pytest.raises(ValidationError):
        client.put(
            f"{prefix}{forms}/{mock.form1.slug}",
            json=body,
        )

    # assert response.status_code == 422
    # res = response.json()
    # assert len(res["detail"]) == 1
    # assert res["detail"][0]["loc"]["source"] == "body"
    # assert res["detail"][0]["loc"]["field"] == "slug"
    # assert res["detail"][0]["msg"] == "none is not an allowed value"
    # assert res["detail"][0]["type"] == "type_error.none.not_allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with a slug that exceeds 255 kars.
# TODO: add constraint in models.py
# ---------------------------------------------
def test_put_forms_slug_exceeds_max_length(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": "formulierrneww42345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
    }

    for id in [
        mock.form1.id,
        # mock.form1.slug,  TODO implement this.
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # TODO 422
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["slug"] == body["slug"]
        assert res["url"] is not None
        assert res["status"] is not None
        assert res["form"] is not None
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None

        # res = response.json()
        # assert len(res) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "slug"
        # assert res["detail"][0]["msg"] == "ensure this value has at most 255 characters"  # noqa: E501
        # assert res["detail"][0]["ctx"]["limit_value"] == 255


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a norm with multiple slugs.
# ---------------------------------------------
def test_put_forms_by_id_multiple_slugs(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        json=body,
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] is not None
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] is not None
    assert res["form"] is not None
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a norm with multiple slugs.
# ---------------------------------------------
def test_put_forms_by_slug_multiple_slugs(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": "test_post_forms_multiple_slugs 1",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 2",  # noqa: F601
        "slug": "test_post_forms_multiple_slugs 3",  # noqa: F601
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.slug}",
        json=body,
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 10
    assert res["id"] is not None
    assert res["name"] is not None
    assert res["slug"] is not None
    assert res["slug"] != body["slug"]
    assert res["url"] is not None
    assert res["status"] is not None
    assert res["form"] is not None
    assert res["config"] is None
    assert res["organizationId"] is None
    assert res["organizationUrl"] is None
    assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where slug already exists
# ---------------------------------------------
def test_put_forms_by_id_slug_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": mock.form6.slug,
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        json=body,
    )

    assert response.status_code == 409
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Conflict"
    assert (
        res["detail"][0]["message"]
        == f"Er bestaat al een formulier met deze slug ({mock.form6.slug}). Geef het formulier een unieke slug."  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_unique"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form where slug already exists
# ---------------------------------------------
def test_put_forms_by_slug_slug_exists(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "slug": mock.form6.slug,
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.slug}",
        json=body,
    )

    assert response.status_code == 409
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Conflict"
    assert (
        res["detail"][0]["message"]
        == f"Er bestaat al een formulier met deze slug ({mock.form6.slug}). Geef het formulier een unieke slug."  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_unique"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled url.
# TODO: check format url? (https/http/...)
# ---------------------------------------------
@pytest.mark.parametrize(
    "url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "11112272345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_put_forms_with_url(
    client: TestClient,
    test_db: Session,
    url_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "url": f"{url_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["url"] == body["url"]
        assert res["status"] is not None
        assert res["form"] is not None
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with status inactive.
# ---------------------------------------------
def test_put_forms_status_inactive(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": mock.statusInactive,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] is not None
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with status active.
# ---------------------------------------------
def test_put_forms_status_active(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": mock.statusActive,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] is not None
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is set to application/json.
# Test update a form with status active.
# ---------------------------------------------
def test_put_forms_status_active_with_content_type_json(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": "application/json",
    }

    body = {
        "status": mock.statusActive,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
            headers=headers,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] is not None
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] is not None
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with invalid status value.
# ---------------------------------------------
@pytest.mark.parametrize(
    "status_invalid_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        "INVALID_ACTIVE",
        "active",
        "inactive",
        None,
    ],
)
def test_put_forms_status_invalid_value(
    client: TestClient,
    test_db: Session,
    status_invalid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": f"{status_invalid_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 422
        res = response.json()
        assert len(res) == 1
        assert len(res["detail"][0]["ctx"]["enum_values"]) == 2
        assert res["detail"][0]["ctx"]["enum_values"][0] == mock.statusActive
        assert res["detail"][0]["ctx"]["enum_values"][1] == mock.statusInactive
        assert res["detail"][0]["loc"]["field"] == "status"
        assert res["detail"][0]["loc"]["source"] == "body"
        assert (
            res["detail"][0]["msg"]
            == "value is not a valid enumeration member; permitted: 'ACTIVE', 'INACTIVE'"  # noqa: E501
        )
        assert res["detail"][0]["type"] == "type_error.enum"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with empty forms field
# TODO is empty value valid?
# ---------------------------------------------
def test_put_forms_form_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "forms": "",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # TODO 422


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with forms field value none
# TODO is None value valid?
# ---------------------------------------------
def test_put_forms_form_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        body = {
            "forms": None,
        }

        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # TODO 422
        # res = response.json()
        # assert len(res["detail"]) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "name"
        # assert res["detail"][0]["msg"] == "field required"
        # assert res["detail"][0]["type"] == "value_error.missing"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update form with additional fields.
# ---------------------------------------------
def test_put_forms_with_additional_fields(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "notvalid1": "formulier new 2",
        "notvalid2": mock.statusActive,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with an invalid organization id.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_organization_id",
    [
        "",
        " ",
        "123",
    ],
)
def test_put_forms_organization_id_not_valid(
    client: TestClient,
    test_db: Session,
    invalid_organization_id: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "organizationId": f"{invalid_organization_id}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )
        assert response.status_code == 422
        res = response.json()
        assert len(res) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "organizationId"
        assert res["detail"][0]["msg"] == "value is not a valid uuid"
        assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with an none organization id.
# TODO 201 -> 422
# ---------------------------------------------
def test_put_forms_organization_id_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "organizationId": None,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200  # 422
        # res = response.json()
        # assert len(res) == 1
        # assert res["detail"][0]["loc"]["source"] == "body"
        # assert res["detail"][0]["loc"]["field"] == "organizationId"
        # assert res["detail"][0]["msg"] == "value is not a valid uuid"
        # assert res["detail"][0]["type"] == "type_error.uuid"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# Request header content type is not set.
# Test update a form with a valid organization id.
# ---------------------------------------------
@pytest.mark.parametrize(
    "valid_organization_id",
    [
        "12345678-1234-4123-a123-1234567890ab",
        "00000000-0000-4000-a000-000000000000",
        "aaaaaaaa-aaaa-4aaa-aaaa-aaaaaaaaaaaa",
    ],
)
def test_put_forms_organization_id_valid(
    client: TestClient,
    test_db: Session,
    valid_organization_id: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "organizationId": f"{valid_organization_id}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["organizationId"] == body["organizationId"]


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled config.
# ---------------------------------------------
@pytest.mark.parametrize(
    "config_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_put_forms_config(
    client: TestClient,
    test_db: Session,
    config_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_config",
        "status": mock.statusActive,
        "form": "{}",
        "config": f"{config_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] == body["name"]
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] == body["config"]
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a prefilled organizationUrl.
# ---------------------------------------------
@pytest.mark.parametrize(
    "organization_url_value",
    [
        "..;/",
        "",
        "   ",
        "1233344",
        "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456",  # noqa: E501
        "http://localhost",
        None,
    ],
)
def test_put_forms_organization_url(
    client: TestClient,
    test_db: Session,
    organization_url_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_organization_url",
        "status": mock.statusInactive,
        "form": "{}",
        "organizationUrl": f"{organization_url_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] == body["name"]
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] == body["organizationUrl"]
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with an empty publishDate.
# ---------------------------------------------
def test_put_forms_publish_date_is_empty(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_is_empty",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": "",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] == body["name"]
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with publishDate set to None.
# ---------------------------------------------
def test_put_forms_publish_date_is_none(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_is_none",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] == body["name"]
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] is None


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with an invalid publishDate.
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_invalid_value",
    [
        "..;/",
        "0000-00-00",
        "0000-00-01",
        "0000-01-00",
        "0000-01-01",
        "01-01-2020",
        "2021-01-00",
        "2021-00-01",
        "0000-00-00",
        "2021-000-01",
        "2021-01-00",
        "2021-13-01",
        "2021-00-00",
        "2021-01-32",
        "2021-01",
        "2021-01-",
        "   ",
        "http://localhost",
    ],
)
def test_put_forms_publish_date_invalid_value(
    client: TestClient,
    test_db: Session,
    publish_date_invalid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_invalid_value",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": f"{publish_date_invalid_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 422
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["loc"]["source"] == "body"
        assert res["detail"][0]["loc"]["field"] == "publishDate"
        assert res["detail"][0]["msg"] == "invalid date format"
        assert res["detail"][0]["type"] == "value_error.date"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# No request header content type is set.
# Test update a form with a valid publishDate.
# TODO: check date format
# ---------------------------------------------
@pytest.mark.parametrize(
    "publish_date_valid_value",
    [
        "0001-01-01",
        "2001-01-01",
        "2021-12-31",
        # "0", -> 1970-01-01
        # "2021", -> 1970-01-01
        # "1233344", -> 1970-01-15
        # "72345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456123456", -> 9999-12-31 # noqa: E501
    ],
)
def test_put_forms_publish_date_valid_value(
    client: TestClient,
    test_db: Session,
    publish_date_valid_value: str,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "test_put_forms_publish_date_valid_value",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": f"{publish_date_valid_value}",
    }

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.put(
            f"{prefix}{forms}/{id}",
            json=body,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] is not None
        assert res["name"] == body["name"]
        assert res["slug"] is not None
        assert res["url"] is not None
        assert res["status"] == body["status"]
        assert res["form"] == body["form"]
        assert res["config"] is None
        assert res["organizationId"] is None
        assert res["organizationUrl"] is None
        assert res["publishDate"] == body["publishDate"]
