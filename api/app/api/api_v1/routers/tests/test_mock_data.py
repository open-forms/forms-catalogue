#!/usr/bin/env python3

from app.database import models
from app.database.models import Status
from collections import namedtuple
from datetime import date
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    [
        "form1",
        "form2",
        "form3",
        "form4",
        "form5",
        "form6",
        "form7",
        "form8",
        "statusInactive",
        "statusActive",
    ],
)


#  Same data als initial_data.
def add_mock_data_to_db(db: Session) -> MockData:

    form1 = models.Form(
        id=None,
        name="formulier a",
        slug="formulier-a",
        url="http://formulieren.openformulieren.io/forms/"
        + "c16ae581-8d6f-424e-bff3-3a70a1374f01",
        status=Status.INACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=None,
    )
    form2 = models.Form(
        id=None,
        name="formulier b",
        slug="formulier-b",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b2",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=None,
    )
    form3 = models.Form(
        id=None,
        name="formulier c",
        slug="formulier-c",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b3",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=None,
    )
    form4 = models.Form(
        id=None,
        name="formulier d",
        slug="formulier-d",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b4",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=None,
    )
    form5 = models.Form(
        id=None,
        name="formulier e",
        slug="formulier-e",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b5",
        status=Status.INACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=None,
    )
    form6 = models.Form(
        id=None,
        name="formulier f2",
        slug="formulier-f",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b6",
        status=Status.INACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=date(2011, 11, 4),
    )
    form7 = models.Form(
        id=None,
        name="formulier g2",
        slug="formulier-g",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b7",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=date(2021, 8, 24),
    )
    form8 = models.Form(
        id=None,
        name="formulier h2",
        slug="formulier-h",
        url="http://formulieren.openformulieren.io/forms/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b8",
        status=Status.ACTIVE,
        form="{}",
        config=None,
        organization_id=None,
        organization_url=None,
        publish_date=date(2111, 11, 4),
    )

    db.add(form1)
    db.add(form2)
    db.add(form3)
    db.add(form4)
    db.add(form5)
    db.add(form6)
    db.add(form7)
    db.add(form8)

    db.flush()

    statusInactive = Status.INACTIVE
    statusActive = Status.ACTIVE

    return MockData(
        form1,
        form2,
        form3,
        form4,
        form5,
        form6,
        form7,
        form8,
        statusInactive,
        statusActive,
    )
