from app.api.api_v1.routers.tests.test_mock_data_css import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
css = "/css"


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_css_list(
    client: TestClient,
    test_db: Session,
):
    """Test get all the css"""
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{css}",
    )
    assert response.status_code == 200
    headers = response.headers
    assert len(headers) == 4
    assert headers["content-length"] == "366"
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-1/2"
    assert headers["x-total-count"] == "results 0-1/2"

    res = response.json()
    assert len(res) == 2

    # Check if id is generated.
    assert len(res[0]) == 8
    assert res[0]["id"] is not None
    assert res[0]["name"] is not None
    assert res[0]["primaryColor"] is not None
    assert res[0]["secondaryColor"] is not None
    assert res[0]["font"] is not None
    assert res[0]["colorLeft"] is not None
    assert res[0]["backgroundColor"] is not None
    assert res[0]["logo"] is not None

    assert len(res[1]) == 8
    assert res[0]["id"] is not None
    assert res[0]["name"] is not None
    assert res[0]["primaryColor"] is not None
    assert res[0]["secondaryColor"] is not None
    assert res[0]["font"] is not None
    assert res[0]["colorLeft"] is not None
    assert res[0]["backgroundColor"] is not None
    assert res[0]["logo"] is not None


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post_method_css(
    client: TestClient,
):

    # Test add a new css
    body = {
        "id": "testId1",
        "name": "css new 1",
        "primary_color": "blue",
        "secondary_color": "black",
        "font": "font1",
        "color_left": "color_left",
        "background_color": "background_color",
        "logo": "logo",
    }

    response = client.post(
        f"{prefix}{css}",
        json=body,
    )

    res = response.json()
    assert response.status_code == 201
    assert len(res) == 8
    assert res["id"] is not None


# ---------------------------------------------
# Testing an API with DELETE /{id} requests
# ---------------------------------------------
def test_delete_method_css(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{css}/{mock.css1.id}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 1
    assert res["message"] == f"Css met id: {mock.css1.id} is verwijderd."


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# ---------------------------------------------
def test_put_method_css(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    # Test update of a css
    body = {
        "color_left": mock.css1.secondary_color,
        "secondary_color": mock.css1.color_left,
    }

    response = client.put(
        f"{prefix}{css}/{mock.css1.id}",
        json=body,
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8


# ---------------------------------------------
# Get "/css/{id}"
# ---------------------------------------------
def test_get_css_by_id(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response1 = client.get(
        f"{prefix}{css}/{mock.css1.id}",
    )

    assert response1.status_code == 200
    res = response1.json()
    assert len(res) == 8

    # Check if id is generated.
    assert res["id"] is not None
    assert res["name"] is not None
    assert res["primaryColor"] is not None
    assert res["secondaryColor"] is not None
    assert res["font"] is not None
    assert res["colorLeft"] is not None
    assert res["backgroundColor"] is not None
    assert res["logo"] is not None
