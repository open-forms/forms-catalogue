import pytest
import requests
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"
form = "/form"


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_forms_list_request_without_request_header_content_type(
    client: TestClient,
):
    response = client.get(
        f"{prefix}{forms}",
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 4
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None
    assert response_headers["x-total-count"] is not None


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_forms_list_request_with_request_header_content_type_application_json(  # noqa: E501
    client: TestClient,
    http_request_header_accept: str,
):

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    response = client.get(
        f"{prefix}{forms}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 4
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None
    assert response_headers["x-total-count"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_forms_list_request_with_request_header_content_type_with_invalid_value(  # noqa: E501
    client: TestClient,
    http_request_header_content_type: str,
):
    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    response = client.get(
        f"{prefix}{forms}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 4
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None
    assert response_headers["content-range"] is not None
    assert response_headers["x-total-count"] is not None


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_one_forms_request_without_request_header_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}/{mock.form1.id}",
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_one_forms_request_with_request_header_content_type_application_json(  # noqa: E501
    client: TestClient,
    http_request_header_accept: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    response = client.get(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_one_forms_request_with_request_header_content_type_with_invalid_value(  # noqa: E501
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    response = client.get(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request without a request content type.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
def test_get_one_form_request_without_request_header_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form7.id}",
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with a request content type application/json.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_get_one_form_request_with_request_header_content_type_application_json(  # noqa: E501
    client: TestClient,
    http_request_header_accept: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    response = client.get(
        f"{prefix}{form}/{mock.form7.id}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_get_one_form_request_with_request_header_content_type_with_invalid_value(  # noqa: E501
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    response = client.get(
        f"{prefix}{form}/{mock.form7.id}",
        headers=headers,
    )

    assert response.status_code == 200

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test get request with an invalid request content type.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        " application/javascript",
        " application/octet-stream",
        " multipart/form-data",
        " text/html",
        " text/plain",
        " ",
        " not valid",
    ],
)
def test_get_one_form_request_with_request_header_content_type_with_invalid_value_2(  # noqa: E501
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    # requests.exceptions.InvalidHeader: Invalid return character or leading space in header: Content-Type # noqa: E501
    with pytest.raises(requests.exceptions.InvalidHeader):
        client.get(
            f"{prefix}{forms}/{mock.form1.id}",
            headers=headers,
        )


# ---------------------------------------------
# Test post request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
def test_post_request_without_request_header_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "name": "formulier new 1",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201  # TODO 406 of 415
    res = response.json()
    assert len(res) == 10

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with a request content type application/json.
# The body contains JSON data.
# ---------------------------------------------
# TODO Reject the request (ideally with a 406 Not Acceptable response) if the
# Accept header does not contain the allowed type application/json.
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_application_json(
    client: TestClient,
    http_request_header_accept: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    body = {
        "name": "formulier new 1",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        headers=headers,
        json=body,
    )

    assert response.status_code == 201  # TODO 406
    res = response.json()
    assert len(res) == 10

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_post_request_with_request_header_content_type_with_invalid_value(
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    body = {
        "name": "formulier new 1",
        "status": mock.statusInactive,
        "form": "{}",
        "publishDate": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        headers=headers,
        json=body,
    )

    assert response.status_code == 422  # TODO 406 or 415
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "body"
    assert res["detail"][0]["msg"] == "value is not a valid dict"
    assert res["detail"][0]["type"] == "type_error.dict"

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test put request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
def test_put_request_without_request_header_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    body = {
        "status": mock.statusInactive,
        "form": "{test_put_method}",
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        json=body,
    )

    assert response.status_code == 200  # TODO 406 of 415
    res = response.json()
    assert len(res) == 10

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test put request with a request content type application/json.
# The body contains JSON data.
# ---------------------------------------------
# TODO Reject the request (ideally with a 406 Not Acceptable response) if the
# Accept header does not contain the allowed type application/json.
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_put_request_with_request_header_content_type_application_json(
    client: TestClient,
    http_request_header_accept: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    body = {
        "status": mock.statusInactive,
        "form": "{test_put_method}",
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
        json=body,
    )

    assert response.status_code == 200  # TODO 406
    res = response.json()
    assert len(res) == 10

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test put request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_put_request_with_request_header_content_type_with_invalid_value(
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    body = {
        "status": mock.statusInactive,
        "form": "{test_put_method}",
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
        json=body,
    )

    assert response.status_code == 422  # TODO 406 or 415
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert len(res["detail"][0]["loc"]) == 1
    assert res["detail"][0]["loc"][0] == "body"
    assert res["detail"][0]["msg"] == "value is not a valid dict"
    assert res["detail"][0]["type"] == "type_error.dict"

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request without a request content type.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
def test_delete_request_without_request_header_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{forms}/{mock.form1.id}",
    )

    assert response.status_code == 200  # TODO 406 of 415
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with a request content type application/json.
# ---------------------------------------------
# TODO Reject the request (ideally with a 406 Not Acceptable response) if the
# Accept header does not contain the allowed type application/json.
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_application_json(
    client: TestClient,
    http_request_header_accept: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }

    response = client.delete(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
    )

    assert response.status_code == 200  # TODO 406
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test delete request with an invalid request content type.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_delete_request_with_request_header_content_type_with_invalid_value(
    client: TestClient,
    http_request_header_content_type: str,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }

    response = client.delete(
        f"{prefix}{forms}/{mock.form1.id}",
        headers=headers,
    )

    assert response.status_code == 200  # TODO 406 or 415
    res = response.json()
    assert len(res) == 1

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None
