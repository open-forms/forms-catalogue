from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endoints
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"
css = "/css"


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the forms.
# Empty list.
# ---------------------------------------------
def test_get_method_forms_list_empty(
    client: TestClient,
):

    response = client.get(
        f"{prefix}{forms}",
    )
    assert response.status_code == 200
    res = response.json()
    assert len(res) == 0


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the forms.
# List is not emtpy.
# ---------------------------------------------
def test_get_method_forms_list(
    client: TestClient,
    test_db: Session,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}",
    )
    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8


# ---------------------------------------------
# Testing an API with POST requests
# ---------------------------------------------
def test_post_method_forms(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    # Test add a new active form
    body = {
        "name": "formulier new 2",
        "status": mock.statusActive,
        "form": "{}",
        "publish_date": None,
    }

    response = client.post(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 201
    res = response.json()
    assert len(res) == 10


# ---------------------------------------------
# Testing an API with DELETE /{id} requests
# ---------------------------------------------
def test_delete_method_forms_with_path_parameter(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{forms}/{mock.form1.id}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 1
    assert (
        res["message"] == f"Formulier met id: {mock.form1.id} is verwijderd."
    )


# ---------------------------------------------
# Testing an API with a DELETE request
# without path parameter id.
# Reject request with HTTP response code 405 Method not allowed
# ---------------------------------------------
def test_delete_method_forms_without_path_parameter(
    client: TestClient,
):

    body = {
        "form": "{}",
    }

    response = client.delete(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 405
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"


# ---------------------------------------------
# Testing an API with HEAD requests
# Reject request with HTTP response code 405 Method not allowed
# ---------------------------------------------
def test_head_method_forms(
    client: TestClient,
):

    response = client.head(
        f"{prefix}{forms}",
    )

    assert response.status_code == 405


# ---------------------------------------------
# Testing an API with PATCH requests
# Reject request with HTTP response code 405 Method not allowed
# ---------------------------------------------
def test_patch_method_forms(client: TestClient):

    response = client.patch(
        f"{prefix}{forms}",
    )

    assert response.status_code == 405
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"


# ---------------------------------------------
# Testing an API with OPTIONS requests
# Reject request with HTTP response code 405 Method not allowed
# ---------------------------------------------
def test_options_method_forms(
    client: TestClient,
):

    response = client.options(
        f"{prefix}{forms}",
    )

    assert response.status_code == 405
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"


# ---------------------------------------------
# Testing an API with PUT/{id} requests
# ---------------------------------------------
def test_put_method_forms_with_path_parameter(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    # Test add a new active form
    body = {
        "status": mock.statusActive,
        "form": "{}",
    }

    response = client.put(
        f"{prefix}{forms}/{mock.form1.id}",
        json=body,
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 10


# ---------------------------------------------
# Testing an API with a PUT request
# without path parameter id.
# Reject request with HTTP response code 405 Method not allowed
# ---------------------------------------------
def test_put_method_forms_without_path_parameter(
    client: TestClient,
):

    # Test add a new active form
    body = {
        "form": "{}",
    }

    response = client.put(
        f"{prefix}{forms}",
        json=body,
    )

    assert response.status_code == 405
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Method Not Allowed"
