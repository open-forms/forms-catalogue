import pytest
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from app.core.types import SemanticError
from fastapi.testclient import TestClient
from pydantic import ValidationError
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"


# ---------------------------------------------
# Testing an API with GET requests
# ---------------------------------------------
def test_get_forms_list(
    client: TestClient,
    test_db: Session,
):
    """Test get all the forms"""
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}",
    )

    assert response.status_code == 200
    headers = response.headers
    assert len(headers) == 4
    assert headers["content-length"] == "2314"
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == "results 0-7/8"
    assert headers["x-total-count"] == "results 0-7/8"

    res = response.json()
    assert len(res) == 8

    # Check if id is generated.
    assert len(res[0]) == 10
    assert res[0]["id"] is not None
    assert res[0]["name"] is not None
    assert res[0]["slug"] is not None
    assert res[0]["url"] is not None
    assert res[0]["status"] is not None
    assert res[0]["form"] is not None
    assert res[0]["config"] is None
    assert res[0]["organizationId"] is None
    assert res[0]["organizationUrl"] is None
    assert res[0]["publishDate"] is None

    assert len(res[1]) == 10
    assert res[1]["id"] is not None
    assert res[1]["name"] is not None
    assert res[1]["slug"] is not None
    assert res[1]["url"] is not None
    assert res[1]["status"] is not None
    assert res[1]["form"] is not None
    assert res[1]["config"] is None
    assert res[1]["organizationId"] is None
    assert res[1]["organizationUrl"] is None
    assert res[1]["publishDate"] is None

    assert len(res[2]) == 10
    assert len(res[3]) == 10
    assert len(res[4]) == 10
    assert len(res[5]) == 10
    assert len(res[6]) == 10
    assert len(res[7]) == 10


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by name. Sort order is ascending
# ---------------------------------------------
def test_get_forms_list_sort_by_name_asc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=name&_order=ASC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["name"] == mock.form1.name
    assert res[1]["name"] == mock.form2.name
    assert res[2]["name"] == mock.form3.name
    assert res[3]["name"] == mock.form4.name
    assert res[4]["name"] == mock.form5.name
    assert res[5]["name"] == mock.form6.name
    assert res[6]["name"] == mock.form7.name
    assert res[7]["name"] == mock.form8.name


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by name. Sort order is descending
# ---------------------------------------------
def test_get_forms_list_sort_by_name_desc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=name&_order=DESC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["name"] == mock.form8.name
    assert res[1]["name"] == mock.form7.name
    assert res[2]["name"] == mock.form6.name
    assert res[3]["name"] == mock.form5.name
    assert res[4]["name"] == mock.form4.name
    assert res[5]["name"] == mock.form3.name
    assert res[6]["name"] == mock.form2.name
    assert res[7]["name"] == mock.form1.name


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by slug. Sort order is ascending
# ---------------------------------------------
def test_get_forms_list_sort_by_slug_asc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=slug&_order=ASC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["slug"] == mock.form1.slug
    assert res[1]["slug"] == mock.form2.slug
    assert res[2]["slug"] == mock.form3.slug
    assert res[3]["slug"] == mock.form4.slug
    assert res[4]["slug"] == mock.form5.slug
    assert res[5]["slug"] == mock.form6.slug
    assert res[6]["slug"] == mock.form7.slug
    assert res[7]["slug"] == mock.form8.slug


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by slug. Sort order is ascending
# ---------------------------------------------
def test_get_forms_list_sort_by_slug_desc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=slug&_order=DESC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["slug"] == mock.form8.slug
    assert res[1]["slug"] == mock.form7.slug
    assert res[2]["slug"] == mock.form6.slug
    assert res[3]["slug"] == mock.form5.slug
    assert res[4]["slug"] == mock.form4.slug
    assert res[5]["slug"] == mock.form3.slug
    assert res[6]["slug"] == mock.form2.slug
    assert res[7]["slug"] == mock.form1.slug


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by status. Sort order is ascending
# ---------------------------------------------
def test_get_forms_list_sort_by_status_asc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=status&_order=ASC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["status"] == mock.form2.status
    assert res[1]["status"] == mock.form3.status
    assert res[2]["status"] == mock.form4.status
    assert res[3]["status"] == mock.form7.status
    assert res[4]["status"] == mock.form8.status
    assert res[5]["status"] == mock.form1.status
    assert res[6]["status"] == mock.form5.status
    assert res[7]["status"] == mock.form6.status


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by status. Sort order is descending
# ---------------------------------------------
def test_get_forms_list_sort_by_status_desc(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?_sort=status&_order=DESC",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8
    assert res[0]["status"] == mock.form1.status
    assert res[1]["status"] == mock.form5.status
    assert res[2]["status"] == mock.form6.status
    assert res[3]["status"] == mock.form7.status
    assert res[4]["status"] == mock.form8.status
    assert res[5]["status"] == mock.form2.status
    assert res[6]["status"] == mock.form3.status
    assert res[7]["status"] == mock.form4.status


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid field. Sort order is ascending
# ---------------------------------------------
def test_get_forms_list_sort_by_invalid_field_asc(
    client: TestClient,
):

    # app.core.types.SemanticError: [{'loc': ('unkownfield',), 'msg': 'extra fields not permitted', 'type': 'value_error.extra'}] # noqa: E501
    with pytest.raises(SemanticError):
        client.get(
            f"{prefix}{forms}?_sort=unkownfield&_order=ASC",
        )


# ---------------------------------------------
# Testing an API with GET requests.
# Sort by invalid field. Sort order is asscending
# ---------------------------------------------
def test_get_forms_list_sort_by_invalid_field_desc(
    client: TestClient,
):

    # app.core.types.SemanticError: [{'loc': ('name',), 'msg': "value is not a valid enumeration member; permitted: 'ASC', 'DESC'", 'type': 'type_error.enum', 'ctx': {'enum_values': [<SortOrder.asc: 'ASC'>, <SortOrder.desc: 'DESC'>]}}] # noqa: E501
    with pytest.raises(SemanticError):
        client.get(
            f"{prefix}{forms}?_sort=name&_order=LEFT",
        )


# ---------------------------------------------
# Testing an API with GET requests.
# Test sort and order combinations.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "_sort=&_order=ASC",
        "_sort=name&_order=",
        "_sort=&_order=",
        "_sort=&_order=",
        "_sort=",
        "_sort=name",
        "_sort=unknownfield",
        "_sort=&_order=",
        "_order=",
        "_order=ASC",
        "_order=DESC",
        "_order=TEST",
    ],
)
def test_get_forms_list_sort_order_combinations(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range without end range.
# Ignore the start range because the end range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "_start=0",
        "_start=1",
        "_start=4",
        "_start=100",
        "_start=",
        "_start=aa",
        "_start=-1",
    ],
)
def test_get_forms_list_range_with_start_without_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8


# ---------------------------------------------
# Testing an API with GET requests.
# Test start range with an end range.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, response_length, content_length, header_content_range",  # noqa: E501
    [
        ("_start=0&_end=4", 4, "1143", "results 0-3/8"),
        ("_start=1&_end=3", 2, "571", "results 1-2/8"),
        ("_start=3&_end=3", 0, "2", "results 3-2/8"),
    ],
)
def test_get_forms_list_range_with_start_with_end(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    response_length: int,
    content_length: str,
    header_content_range: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == int(f"{response_length}")

    headers = response.headers
    assert len(headers) == 4
    assert headers["content-length"] == f"{content_length}"
    assert headers["content-type"] == "application/json"
    assert headers["content-range"] == f"{header_content_range}"
    assert headers["x-total-count"] == f"{header_content_range}"


# ---------------------------------------------
# Testing an API with GET requests.
# Test invalid start range an end range combinations.
# ---------------------------------------------
def test_get_forms_list_range_with_invalid_start_end_combination(
    client: TestClient,
    test_db: Session,
):
    add_mock_data_to_db(test_db)

    # test start with an empty end range
    response1 = client.get(
        f"{prefix}{forms}?_start=3&_end=",
    )

    assert response1.status_code == 200
    res = response1.json()
    assert len(res) == 8

    # test empty start with an empty end range
    response2 = client.get(
        f"{prefix}{forms}?_start=&_end=",
    )

    assert response2.status_code == 200
    res = response2.json()
    assert len(res) == 8

    # test range end cannot be before start
    # pydantic.error_wrappers.ValidationError: 1 validation error for RangeOptions # noqa: E501
    # end
    #   range end cannot be before start (type=assertion_error)
    with pytest.raises(ValidationError):
        client.get(
            f"{prefix}{forms}?_start=3&_end=2",
        )

    # test end value is greater than or equal to 0
    # pydantic.error_wrappers.ValidationError: 1 validation error for RangeOptions # noqa: E501
    # end
    #   ensure this value is greater than or equal to 0 (type=value_error.number.not_ge; limit_value=0) # noqa: E501
    with pytest.raises(ValidationError):
        client.get(
            f"{prefix}{forms}?_start=0&_end=-1",
        )

    # test end value is not a valid integer
    # pydantic.error_wrappers.ValidationError: 1 validation error for RangeOptions # noqa: E501
    # end
    # value is not a valid integer (type=type_error.integer)
    with pytest.raises(ValidationError):
        client.get(
            f"{prefix}{forms}?_start=0&_end=aabbcc",
        )

    # test start and end value are not a valid integer
    # pydantic.error_wrappers.ValidationError: 2 validation errors for RangeOptions # noqa: E501
    # start
    #   value is not a valid integer (type=type_error.integer)
    # end
    #   value is not a valid integer (type=type_error.integer)
    with pytest.raises(ValidationError):
        client.get(
            f"{prefix}{forms}?_start=aa&_end=bb",
        )

    # test page size cannot exceed 100
    # pydantic.error_wrappers.ValidationError: 1 validation error for RangeOptions # noqa: E501
    # end
    #   page size cannot exceed 100 (type=assertion_error)
    with pytest.raises(ValidationError):
        client.get(
            f"{prefix}{forms}?_start=1111111111111111111111111111111&_end=1111111111111111111111111111111222222222222222222222222222222222222222222222",  # noqa: E501
        )


# ---------------------------------------------
# Testing an API with GET requests.
# Test end range without start range.
# Ignore the end range because the start range is not set.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "_end=0",
        "_end=1",
        "_end=4",
        "_end=100",
        "_end=",
        "_end=aa",
        "_end=-1",
    ],
)
def test_get_forms_list_range_with_end_without_start(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 8


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search.
# fulltext fields  are name and slug
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, results",
    [
        ("q=", 8),
        ("q=aaaaaaaaaaaaa", 0),
        ("q=formulier ", 8),  # form name
        ("q=FoRmUliEr ", 8),  # form name
        ("q=formulier g", 1),  # form name
        ("q=formulier-", 8),  # slug
        ("q=fOrMuLiEr-", 8),  # slug
        ("q=formulier-g", 1),  # slug
        ("q=formulier", 8),  # form name and slug
        ("q=formulieren", 0),  # url
        ("q=ACTIVE", 0),
        ("q=active", 0),
        ("q=INACTIVE", 0),
        ("q=t", 0),
        ("q=T", 0),
    ],
)
def test_get_forms_list_full_text_filter(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    results: int,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == results


# ---------------------------------------------
# Testing an API with GET requests.
# Test filter option for field Status.
# TODO fid sqlalchemy.exc.DataError: (psycopg2.errors.InvalidTextRepresentation) invalid input value for enum status: # noqa: E501
# ---------------------------------------------
@pytest.mark.parametrize(
    "filter_query_parameter, results",
    [
        ("status=INACTIVE", 3),
        ("status=ACTIVE", 5),
        ("status=", 8),
        # ("status=None", 8), # TODO fix DataError
        # ("status=iNaCtIvE", 0), # TODO fix DataError
        # ("status=aCtIvE", 0), # TODO fix DataError
        # ("status=in", 0), # TODO fix DataError
        # ("status=ac", 0), # TODO fix DataError
        # ("status=a", 0), # TODO fix DataError
    ],
)
def test_get_forms_list_filter_option_status(
    client: TestClient,
    test_db: Session,
    filter_query_parameter: str,
    results: int,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{filter_query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == results


# ---------------------------------------------
# Testing an API with GET requests.
# Test fulltext search in combination with filter option for field Status.
# Fulltext fields are name and slug
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter, results",
    [
        ("q=&status=INACTIVE", 3),
        ("q=aaaaaaaaaaaaa&status=INACTIVE", 0),
        ("q=formulier &status=INACTIVE", 3),  # form name
        ("q=FoRmUliEr &status=INACTIVE", 3),  # form name
        ("q=formulier g&status=INACTIVE", 0),  # form name
        ("q=formulier-&status=INACTIVE", 3),  # slug
        ("q=fOrMuLiEr-&status=INACTIVE", 3),  # slug
        ("q=formulier-g&status=INACTIVE", 0),  # slug
        ("q=formulier&status=INACTIVE", 3),  # form name and slug
        ("q=formulieren&status=INACTIVE", 0),  # url
        ("q=ACTIVE&status=INACTIVE", 0),
        ("q=active&status=INACTIVE", 0),
        ("q=INACTIVE&status=INACTIVE", 0),
        ("q=t&status=INACTIVE", 0),
        ("q=T&status=INACTIVE", 0),
        ("q=&status=ACTIVE", 5),
        ("q=aaaaaaaaaaaaa&status=ACTIVE", 0),
        ("q=formulier &status=ACTIVE", 5),  # form name
        ("q=FoRmUliEr &status=ACTIVE", 5),  # form name
        ("q=formulier g&status=ACTIVE", 1),  # form name
        ("q=formulier-&status=ACTIVE", 5),  # slug
        ("q=fOrMuLiEr-&status=ACTIVE", 5),  # slug
        ("q=formulier-g&status=ACTIVE", 1),  # slug
        ("q=formulier&status=ACTIVE", 5),  # form name and slug
        ("q=formulieren&status=ACTIVE", 0),  # url
        ("q=ACTIVE&status=ACTIVE", 0),
        ("q=active&status=ACTIVE", 0),
        ("q=INACTIVE&status=ACTIVE", 0),
        ("q=t&status=ACTIVE", 0),
        ("q=T&status=ACTIVE", 0),
    ],
)
def test_get_forms_list_full_text_filter_and_filter_option_status(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
    results: int,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}?{query_parameter}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == results
