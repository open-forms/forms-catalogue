import pytest
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"


# ---------------------------------------------
# Delete "/forms/{id}"
# ---------------------------------------------
def test_delete_forms_without_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form2.id,
        mock.form3.id,
        mock.form4.id,
        mock.form5.slug,
        mock.form6.slug,
        mock.form7.slug,
        mock.form8.slug,
    ]:
        response = client.delete(
            f"{prefix}{forms}/{id}",
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 1
        assert res["message"] == f"Formulier met id: {id} is verwijderd."


# ---------------------------------------------
# Delete "/forms/{id}"
# ---------------------------------------------
def test_delete_forms_with_content_type_application_json(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": "application/json",
    }

    for id in [
        mock.form1.id,
        mock.form2.id,
        mock.form3.id,
        mock.form4.id,
        mock.form5.slug,
        mock.form6.slug,
        mock.form7.slug,
        mock.form8.slug,
    ]:
        response = client.delete(
            f"{prefix}{forms}/{id}",
            headers=headers,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 1
        assert res["message"] == f"Formulier met id: {id} is verwijderd."


# ---------------------------------------------
# Delete "/forms/{id}"
# Test delete form twice.
# ---------------------------------------------
def test_delete_forms_twice(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form2.slug,
    ]:
        response1 = client.delete(
            f"{prefix}{forms}/{id}",
        )
        response2 = client.delete(
            f"{prefix}{forms}/{id}",
        )

        assert response1.status_code == 200
        res1 = response1.json()
        assert len(res1) == 1
        assert res1["message"] == f"Formulier met id: {id} is verwijderd."

        assert response2.status_code == 404
        res2 = response2.json()
        assert len(res2["detail"]) == 1
        assert res2["detail"][0]["name"] == "Not Found"
        assert (
            res2["detail"][0]["message"]
            == f"Er is geen formulier gevonden met {id}"
        )
        assert res2["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "99",
        "-1",
        "99999999999999999999999999999999999999999999999999999999999999",
        "_",
        "aa",
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value(
    client: TestClient,
    test_db: Session,
    invalid_path_parameter_id_value: str,
):
    add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{forms}/{invalid_path_parameter_id_value}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {invalid_path_parameter_id_value}"  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value_2(
    client: TestClient,
    test_db: Session,
    invalid_path_parameter_id_value: str,
):
    add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{forms}/{invalid_path_parameter_id_value}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Not Found"


# ---------------------------------------------
# Delete "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_path_parameter_id_value",
    [
        "..;/",
        "",
        "?id=1 AND 1::int=1",
        "?id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
    ],
)
def test_delete_forms_with_invalid_path_parameter_id_value_3(
    client: TestClient,
    test_db: Session,
    invalid_path_parameter_id_value: str,
):
    add_mock_data_to_db(test_db)

    response = client.delete(
        f"{prefix}{forms}/{invalid_path_parameter_id_value}",
    )

    assert response.status_code == 307  # Temporary Redirect
    headers = response.headers
    assert len(headers) == 1
    assert headers["Location"] is not None
