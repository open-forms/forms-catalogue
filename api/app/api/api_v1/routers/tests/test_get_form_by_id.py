from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
form = "/form"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is inactive and publishdate is not set
# ---------------------------------------------
def test_get_form_status_inactive_publishdate_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form5.slug,
    ]:
        response = client.get(
            f"{prefix}{form}/{id}",
        )

        assert response.status_code == 404
        res = response.json()
        assert len(res["detail"]) == 1
        assert res["detail"][0]["name"] == "Not Found"
        assert (
            res["detail"][0]["message"]
            == f"Er is geen formulier gevonden met {id}"
        )
        assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is inactive and publishdate is set
# ---------------------------------------------
def test_get_form_by_id_status_inactive_publishdate_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form6.id}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form6.id}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is inactive and publishdate is set
# ---------------------------------------------
def test_get_form_by_slug_status_inactive_publishdate_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form6.slug}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form6.slug}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is not set
# ---------------------------------------------
def test_get_form_by_id_status_active_publishdate_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form2.id}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form2.id}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is not set
# ---------------------------------------------
def test_get_form_by_slug_status_active_publishdate_not_set(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form2.slug}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form2.slug}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is in the future
# ---------------------------------------------
def test_get_form_by_id_status_active_publishdate_in_future(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form8.id}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form8.id}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is in the future
# ---------------------------------------------
def test_get_form_by_slug_status_active_publishdate_in_future(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form8.slug}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form8.slug}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is in the past
# ---------------------------------------------
def test_get_form_by_id_status_active_publishdate_in_past(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form7.id}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 10
    assert res["id"] == str(mock.form7.id)
    assert res["name"] == mock.form7.name
    assert res["slug"] == mock.form7.slug
    assert res["url"] == mock.form7.url
    assert res["status"] == mock.form7.status
    assert res["form"] == mock.form7.form
    assert res["config"] == mock.form7.config
    assert res["organizationId"] == mock.form7.organization_id
    assert res["organizationUrl"] == mock.form7.organization_url
    assert res["publishDate"] == str(mock.form7.publish_date)


# ---------------------------------------------
# Get "/form/{id}"
# Test status is active and publishdate is in the past
# ---------------------------------------------
def test_get_form_by_slug_status_active_publishdate_in_past(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{form}/{mock.form7.slug}",
    )

    assert response.status_code == 200
    res = response.json()
    assert len(res) == 10
    assert res["id"] == str(mock.form7.id)
    assert res["name"] == mock.form7.name
    assert res["slug"] == mock.form7.slug
    assert res["url"] == mock.form7.url
    assert res["status"] == mock.form7.status
    assert res["form"] == mock.form7.form
    assert res["config"] == mock.form7.config
    assert res["organizationId"] == mock.form7.organization_id
    assert res["organizationUrl"] == mock.form7.organization_url
    assert res["publishDate"] == str(mock.form7.publish_date)
