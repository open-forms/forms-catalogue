import pytest
from fastapi.testclient import TestClient

# The api endoints
prefix = ""  # TODO "/forms-catalogue/api/v1"
root = ""
forms = "/forms"
css = "/css"


# ---------------------------------------------
# Test get request with root path
# ---------------------------------------------
def test_get_method_for_root_path(client: TestClient):
    """Test get the api root path """

    response = client.get(
        f"{prefix}{root}",
    )
    assert response.status_code == 200
    res = response.json()
    assert len(res) == 4
    assert res["@context"] == "/contexts/Entrypoint"
    assert res["@id"] == "/"
    assert res["@type"] == "Entrypoint"
    assert res["form"] == "/forms"


# ---------------------------------------------
# Test get request with an unknown endpoint.
# ---------------------------------------------
@pytest.mark.parametrize(
    "get_unknown_endpoint",
    [
        "blablabla",
        "not valid",
        "*/*",
        # "",  # the root path
        # None, # the root path
        "/unknown",
        "/..;/",
        "/forms%20%20dfdfdfdfererrdfdf",
    ],
)
def test_get_method_for_unknown_path(
    client: TestClient, get_unknown_endpoint: str
):
    """Test unknown endpoint """

    # Test unkown endpoint
    response = client.get(
        get_unknown_endpoint,
    )
    assert response.status_code == 404
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Not Found"


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the forms.
# ---------------------------------------------
def test_get_method_forms_list(client: TestClient):
    """Test get all the forms """

    response = client.get(
        f"{prefix}{forms}",
    )
    assert response.status_code == 200


# ---------------------------------------------
# Testing the API with a GET requests
# to retreive all the css.
# ---------------------------------------------
def test_get_method_css_list(client: TestClient):
    """Test get all the css """

    response = client.get(
        f"{prefix}{css}",
    )
    assert response.status_code == 200
