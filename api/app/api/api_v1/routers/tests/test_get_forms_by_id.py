import pytest
from app.api.api_v1.routers.tests.test_mock_data import add_mock_data_to_db
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

# The api endpoint
prefix = ""  # TODO "/forms-catalogue/api/v1"
forms = "/forms"


# ---------------------------------------------
# Get "/forms/{id}"
# ---------------------------------------------
def test_get_forms_by_id_without_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form2.id,
        mock.form3.id,
        mock.form4.id,
        mock.form5.id,
        mock.form6.id,
        mock.form7.id,
        mock.form8.id,
    ]:
        response = client.get(
            f"{prefix}{forms}/{id}",
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(id)


# ---------------------------------------------
# Get "/forms/{slug}"
# ---------------------------------------------
def test_get_forms_by_slug_without_content_type(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    for slug in [
        mock.form1.slug,
        mock.form2.slug,
        mock.form3.slug,
        mock.form4.slug,
        mock.form5.slug,
        mock.form6.slug,
        mock.form7.slug,
        mock.form8.slug,
    ]:
        response = client.get(
            f"{prefix}{forms}/{slug}",
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["slug"] == slug


# ---------------------------------------------
# Get "/forms/{id}"
# ---------------------------------------------
def test_get_forms_by_id_with_content_type_application_json(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": "application/json",
    }

    for id in [
        mock.form1.id,
        mock.form2.id,
        mock.form3.id,
        mock.form4.id,
        mock.form5.id,
        mock.form6.id,
        mock.form7.id,
        mock.form8.id,
    ]:
        response = client.get(
            f"{prefix}{forms}/{id}",
            headers=headers,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(id)


# ---------------------------------------------
# Get "/forms/{slug}"
# ---------------------------------------------
def test_get_forms_by_slug_with_content_type_application_json(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    headers = {
        "Content-Type": "application/json",
    }

    for slug in [
        mock.form1.slug,
        mock.form2.slug,
        mock.form3.slug,
        mock.form4.slug,
        mock.form5.slug,
        mock.form6.slug,
        mock.form7.slug,
        mock.form8.slug,
    ]:
        response = client.get(
            f"{prefix}{forms}/{slug}",
            headers=headers,
        )

        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["slug"] == slug


# ---------------------------------------------
# Get "/forms/{id}"
# Test ignore the query parameters.
# ---------------------------------------------
@pytest.mark.parametrize(
    "query_parameter",
    [
        "",
        "unknown",
        "0",
        "__",
        "<>",
        "id=1 AND 1::int=1",
        "?id=1 AND 1::int=1",
        "&id=1 AND 1::int=1",
        "id=1 UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        None,
    ],
)
def test_get_forms_with_addition_query_params(
    client: TestClient,
    test_db: Session,
    query_parameter: str,
):
    mock = add_mock_data_to_db(test_db)

    for id in [
        mock.form1.id,
        mock.form1.slug,
    ]:
        response = client.get(
            f"{prefix}{forms}/{id}?{query_parameter}",
        )
        assert response.status_code == 200
        res = response.json()
        assert len(res) == 10
        assert res["id"] == str(mock.form1.id)
        assert res["name"] == mock.form1.name
        assert res["slug"] == mock.form1.slug
        assert res["url"] == mock.form1.url
        assert res["status"] == mock.form1.status
        assert res["form"] == mock.form1.form
        assert res["config"] == mock.form1.config
        assert res["organizationId"] == mock.form1.organization_id
        assert res["organizationUrl"] == mock.form1.organization_url
        assert res["publishDate"] == mock.form1.publish_date


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        "unknown",
        "0",
        "__",
        "<>",
        "111111111111111111111111111111111111111111111111111111111",
    ],
)
def test_get_forms_with_invalid_id_value(
    client: TestClient,
    test_db: Session,
    invalid_id_value: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}/{invalid_id_value}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"
    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {invalid_id_value}"
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        " AND 1::int=1",
        " UNION ALL SELECT NULL,version(),NULL LIMIT 1 OFFSET 1--",
        " or 1=1",
        " or = 1' or '1' = '1",
        "not valid",
    ],
)
def test_get_forms_with_invalid_id_value_2(
    client: TestClient,
    test_db: Session,
    invalid_id_value: str,
):
    mock = add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}/{mock.form1.id}{invalid_id_value}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"

    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form1.id}{invalid_id_value}"  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
@pytest.mark.parametrize(
    "invalid_id_value",
    [
        ">'\"()%26%25<acx><ScRiPt%20>jLeb(9968)</ScRiPt>",
        "text/plain",
        "application/*",
        "*/*",
    ],
)
def test_get_forms_with_invalid_id_value_3(
    client: TestClient,
    test_db: Session,
    invalid_id_value: str,
):
    add_mock_data_to_db(test_db)

    response = client.get(
        f"{prefix}{forms}/{invalid_id_value}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 9
    assert res["detail"] == "Not Found"


# ---------------------------------------------
# Get "/forms/{id}"
# Test path parameter id with invalid values.
# ---------------------------------------------
def test_get_forms_with_invalid_id_value_4(
    client: TestClient,
    test_db: Session,
):
    mock = add_mock_data_to_db(test_db)

    invalid_id = "..;/"
    invalid_id_test = ".."

    response = client.get(
        f"{prefix}{forms}/{mock.form1.id}{invalid_id}",
    )

    assert response.status_code == 404
    res = response.json()
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Not Found"

    assert (
        res["detail"][0]["message"]
        == f"Er is geen formulier gevonden met {mock.form1.id}{invalid_id_test}"  # noqa: E501
    )
    assert res["detail"][0]["stack"]["type"] == "semantic_error.not_found"
