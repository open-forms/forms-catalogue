"""This module contains all the crud api calls for a form"""

import json
import datetime
import uuid
from typing import List, Union
from fastapi import APIRouter, Depends, Response
from fastapi.exceptions import HTTPException
from fastapi_jwt_auth import AuthJWT
from slugify.main import UniqueSlugify
from sqlalchemy.orm import Session
from app import domain, schemas
from app.core import config
from app.core.rest_helpers import SearchParameters
from app.core.types import SemanticError, SemanticErrorType
from app.database.session import get_database

router = r = APIRouter()

# validate the filter and sort parameters
validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.FormFilterableFields,  # type: ignore
    sort_options_schema=schemas.FormSortableFields,  # type: ignore
)


@r.get("/")
async def root():
    """The root path and entrypoint for this api, showing the subpath /forms"""
    return {
        "@context": "/contexts/Entrypoint",
        "@id": "/",
        "@type": "Entrypoint",
        "openapi": "/api",
        "docs": "/api/docs",
        "redoc": "/api/redoc",
        "form": "/forms",
    }


@r.get(
    "/forms/{id}",
    response_model=schemas.Form,
    responses={404: {"description": "Form not found"}},
)
def get_form(
    id: Union[uuid.UUID, str],
    database: Session = Depends(get_database),
):
    """Retrieve a single form, given its uuid or slug"""
    try:
        form = domain.form.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen formulier gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    return form


# TODO: only used from the 'inwoner app'. Remove when auth0 is added.
@r.get(
    "/form/{id}",
    response_model=schemas.Form,
    responses={404: {"description": "Form not found"}},
)
def get_form_active(
    id: Union[uuid.UUID, str],
    database: Session = Depends(get_database),
):
    """Retrieve a single form, given its unique id or slug"""

    error_content = [
      {
        "name": "Not Found",
        "message": f"Er is geen formulier gevonden met {id}",
        "stack": {"type": SemanticErrorType.NOT_FOUND},
      }
    ]

    try:
        form = domain.form.get(database=database, id=id)
    except SemanticError as semantic_error:
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    if form is None or form.status == "INACTIVE" or \
      (form.publish_date is None or form.publish_date > datetime.date.today()):
        raise HTTPException(status_code=404, detail=error_content)

    return form


@r.get(
    "/forms",
    response_model=List[schemas.Form],
)
def get_form_list(
    response: Response,
    database: Session = Depends(get_database),
    search_parameters: dict = Depends(validate_search_parameters),
):
    """Retrieves a list of forms"""

    search_result = domain.form.get_multi(database=database, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range(
        "results"
    )
    response.headers["X-Total-Count"] = search_result.as_content_range(
        "results"
    )
    return search_result.result


# TODO: don't return a http status 500
@r.post(
    "/forms",
    response_model=schemas.Form,
    status_code=201,
    responses={
        400: {"description": "Bad Request. Form not created."},
        409: {"description": "Conflict. Form not created."},
        500: {"description": "Internal Server Error. Form not created."},
    },
)
def create_form(
    form_create_request: schemas.FormCreateMinimal,
    database: Session = Depends(get_database),
    authorize: AuthJWT = Depends()
):
    """Creates a new form"""

    authorize.jwt_required()

    # Lets make sure a valid form.io json blob form is given, containing at least one component in a list of components
    form = form_create_request.form
    try:
        form_components = json.loads(form)['components']
        if not isinstance(form_components, list) or len(form_components) == 0 or 'type' not in form_components[0]:
            if len(form) > 150:
                form = "\'form\'"
            error_content = [
                {
                    "name": "Bad Request",
                    "message": f"{form} form.io form should at least contain one valid component in a list of "
                               f"components",
                    # type: ignore # noqa: E501 ^
                    "stack": {"type": SemanticErrorType.INVALID_INPUT},
                }
            ]
            raise HTTPException(status_code=400, detail=error_content)
    except ValueError:
        if len(form) > 150:
            form = "\'form\'"
        error_content = [
            {
                "name": "Bad Request",
                "message": f"{form} form.io form is geen geldige json.",
                # type: ignore # noqa: E501 ^
                "stack": {"type": SemanticErrorType.INVALID_INPUT},
            }
        ]
        raise HTTPException(status_code=400, detail=error_content)

    # generate a slug with the name that is slugable
    custom_slugify = UniqueSlugify()
    slug = custom_slugify(form_create_request.name)

    # create the form
    try:
        form = domain.form.create(
            database=database,
            obj_in=schemas.FormCreate(
                name=form_create_request.name,
                status=form_create_request.status,
                form=form_create_request.form,
                config=form_create_request.config,
                organization_id=form_create_request.organization_id,
                organization_url=form_create_request.organization_url,
                slug=slug,
                url="placeholder",
                publish_date=form_create_request.publish_date,
            ),
        )
        # set the url for this form with its new uuid
        url = f"{config.APP_URL}/forms/{form.id}"
        form_result = domain.form.update(
            database=database,
            id=form.id,
            obj_in=schemas.FormUpdate(url=url),
        )
    except SemanticError as semantic_error:
        if "name already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam.",
                    # type: ignore # noqa: E501 ^
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error
        if "slug already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": f"Er bestaat al een formulier met deze slug ({slug}). Geef het formulier "
                               f"een andere naam.",  # type: ignore # noqa: E501
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error
        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Formulier is niet aangemaakt. Er ging iets mis.",
                "stack": {
                    "type": type(semantic_error).__name__,
                    "args": template.format("", semantic_error.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from semantic_error

    return form_result


# TODO: don't return a http status 500
@r.put(
    "/forms/{id}",
    response_model=schemas.Form,
    responses={
        404: {"description": "Form not found"},
        409: {"description": "Conflict. Form not updated."},
        500: {"description": "Internal Server Error. Form not updated."},
    },
)
def update_form(
    id: Union[uuid.UUID, str],
    form_update_request: schemas.FormUpdate,
    database: Session = Depends(get_database),
    authorize: AuthJWT = Depends()
):
    """Update a form, given its unique id or slug"""

    authorize.jwt_required()
    try:
        domain.form.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen formulier gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    # make sure this slug is slugable
    if form_update_request.slug:
        custom_slugify = UniqueSlugify()
        form_update_request.slug = custom_slugify(form_update_request.slug)

    # update the form
    try:
        updated_form = domain.form.update(
            database=database,
            id=id,
            obj_in=form_update_request,
        )
    except SemanticError as semantic_error:
        if "name already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": "Er bestaat al een formulier met deze naam. Geef het formulier een unieke naam.",
                    # type: ignore # noqa: E501 ^
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error
        if "slug already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": f"Er bestaat al een formulier met deze slug ({form_update_request.slug}). Geef het "
                               f"formulier een unieke slug.",  # type: ignore # noqa: E501
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error
        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Formulier is niet gewijzigd. Er ging iets mis.",
                "stack": {
                    "type": type(semantic_error).__name__,
                    "args": template.format("", semantic_error.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from semantic_error

    return updated_form


# TODO: don't return a http status 500
@r.delete(
    "/forms/{id}",
    responses={
        404: {"description": "Form not found"},
        500: {"description": "Internal Server Error. Form not deleted."},
    },
)
def delete_form(
    id: Union[uuid.UUID, str],
    database: Session = Depends(get_database),
    authorize: AuthJWT = Depends()
):
    """Delete a form"""

    authorize.jwt_required()
    try:
        form = domain.form.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen formulier gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    if form is None:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen formulier gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content)

    try:
        domain.form.delete(database=database, id=form.id)
    except Exception as ex:
        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Formulier is niet verwijderd. Er ging iets mis.",
                "stack": {
                    "type": type(ex).__name__,
                    "args": template.format("", ex.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from ex

    return {"message": f"Formulier met id: {id} is verwijderd."}
