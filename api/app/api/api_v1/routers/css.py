"""Module for all css api routes"""

import uuid
from typing import List
from fastapi import APIRouter, Depends, Response
from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from app import domain, schemas
from app.core.rest_helpers import SearchParameters
from app.core.types import SemanticError, SemanticErrorType
from app.database.session import get_database

router = r = APIRouter()

# validate the filter and sort parameters
validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.CssFilterableFields,  # type: ignore
    sort_options_schema=schemas.CssSortableFields,  # type: ignore
)


@r.get(
    "/css/{id}",
    response_model=schemas.Css,
    responses={404: {"description": "Css not found"}},
)
def get_css(
    id: uuid.UUID,
    database: Session = Depends(get_database),
):
    """Retrieve a single css, given its uuid or slug"""
    try:
        css = domain.css.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen css gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    return css


@r.get(
    "/css",
    response_model=List[schemas.Css],
)
def get_css_list(
    response: Response,
    database: Session = Depends(get_database),
    search_parameters: dict = Depends(validate_search_parameters),
):
    """Get a list of css."""

    search_result = domain.css.get_multi(database=database, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range(
        "results"
    )
    response.headers["X-Total-Count"] = search_result.as_content_range(
        "results"
    )
    return search_result.result


# TODO: don't return a http status 500
@r.post(
    "/css",
    response_model=schemas.Css,
    status_code=201,
    responses={
        409: {"description": "Conflict. Css not created."},
        500: {"description": "Internal Server Error. Css not created."},
    },
)
def create_css(
    css_create_request: schemas.CssCreate,
    database: Session = Depends(get_database),
):
    """Add a new css."""

    try:
        css = domain.css.create(
            database=database,
            obj_in=schemas.CssCreate(
                name=css_create_request.name,
                background_color=css_create_request.background_color,
                color_left=css_create_request.color_left,
                font=css_create_request.font,
                logo=css_create_request.logo,
                primary_color=css_create_request.primary_color,
                secondary_color=css_create_request.secondary_color,
            ),
        )
    except SemanticError as semantic_error:
        if "name already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": "Er bestaat al een css met deze naam. Geef een unieke naam.",  # type: ignore # noqa: E501
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error

        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Css is niet aangemaakt. Er ging iets mis.",
                "stack": {
                    "type": type(semantic_error).__name__,
                    "args": template.format("", semantic_error.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from semantic_error

    return css


# TODO: don't return a http status 500
@r.put(
    "/css/{id}",
    response_model=schemas.Css,
    responses={
        404: {"description": "Css not found"},
        409: {"description": "Conflict. Css not updated."},
        500: {"description": "Internal Server Error. Css not updated."},
    },
)
def update_css(
    id: uuid.UUID,
    css_update_request: schemas.CssUpdate,
    database: Session = Depends(get_database),
):
    """Update a css by id"""

    try:
        domain.css.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen css gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    # update the css
    try:
        updated_css = domain.css.update(
            database=database,
            id=id,
            obj_in=css_update_request,
        )
    except SemanticError as semantic_error:
        if "name already exists" in semantic_error.args[0]:
            error_content = [
                {
                    "name": "Conflict",
                    "message": "Er bestaat al een css met deze naam. Geef een unieke naam.",  # type: ignore # noqa: E501
                    "stack": {"type": SemanticErrorType.NOT_UNIQUE},
                }
            ]
            raise HTTPException(status_code=409, detail=error_content) from semantic_error

        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Css is niet gewijzigd. Er ging iets mis.",
                "stack": {
                    "type": type(semantic_error).__name__,
                    "args": template.format("", semantic_error.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from semantic_error

    return updated_css


# TODO: don't return a http status 500
@r.delete(
    "/css/{id}",
    responses={
        404: {"description": "Css not found"},
        500: {"description": "Internal Server Error. Css not deleted."},
    },
)
def delete_css(
    id: uuid.UUID,
    database: Session = Depends(get_database),
):
    """Delete a css by id."""

    try:
        css = domain.css.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen css gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    if css is None:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen css gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content)

    try:
        domain.css.delete(database=database, id=css.id)
    except Exception as ex:
        template = "Arguments:{1!r}"
        error_content = [
            {
                "name": "Internal Server Error",
                "message": "Css is niet verwijderd. Er ging iets mis.",
                "stack": {
                    "type": type(ex).__name__,
                    "args": template.format("", ex.args),
                },
            }
        ]
        raise HTTPException(status_code=500, detail=error_content) from ex

    return {"message": f"Css met id: {id} is verwijderd."}
