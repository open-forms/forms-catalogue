"""Config module with env variables"""

import os

PROJECT_NAME = "forms-catalogue"

# Lets pick up the database from the env variables
SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]

SQLALCHEMY_TRACK_MODIFICATIONS = False

# If set to something true, all database queries will be logged as they are
# executed.
SQLALCHEMY_ECHO = bool(os.environ.get("DATABASE_ECHO", False))

# Maximum number of results from a search. To retrieve more, use paging.
MAX_PAGE_SIZE = int(os.environ.get("MAX_PAGE_SIZE", "100"))

PRIVATE_KEY = os.environ.get("PRIVATE_KEY", "")
PUBLIC_KEY = os.environ.get("PUBLIC_KEY", "")
APP_APPLICATION_KEY = os.environ.get("APP_APPLICATION_KEY", "")

APP_URL = os.environ.get("APP_URL", "")
APP_DOMAIN = os.environ.get("APP_DOMAIN", "")
APP_ENV = os.environ.get("APP_ENV", "")

# Flag to enable or disable audit logging
AUDIT_LOGGING = bool(os.environ.get("AUDIT_LOGGING", True))
