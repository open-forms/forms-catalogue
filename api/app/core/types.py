"""
Types used by multiple parts of the application
"""

import enum
from typing import Any, Dict, List, Optional, Union
from pydantic import BaseModel, ConstrainedInt, validator
from app.core.config import MAX_PAGE_SIZE


class UnsignedInt(ConstrainedInt):
    """pydantic's built-in 'PositiveInt' needs a value > 0; but our use-case requires a constraint >= 0."""
    ge = 0


class FilterOptions(BaseModel):
    """Filter options class"""
    options: Dict


class RangeOptions(BaseModel):
    """Range options class"""
    start: UnsignedInt
    end: UnsignedInt

    @validator("end")
    def _validate_end_after_start(cls, end, values):
        """Validate end is after start"""
        start = values["start"]
        assert start <= end, "range end cannot be before start"
        assert (
            end - start
        ) <= MAX_PAGE_SIZE, f"page size cannot exceed {MAX_PAGE_SIZE}"
        return end


class SortOrder(str, enum.Enum):
    """Sort order enum"""
    ASC = "ASC"
    DESC = "DESC"


class SortOptions(BaseModel):
    """Sort options"""
    field: str
    order: SortOrder
    model_name: Optional[str]


class SearchResult(BaseModel):
    """Search result"""
    result: Any
    start: UnsignedInt
    end: Optional[UnsignedInt]
    total: UnsignedInt

    def as_content_range(self, unit: str):
        """
        Convert the search result's start/end and total to a valid
        Content-Range.

        If there is no end (which happens if there are no results), the range
        will be '*' (which means "unknown").

        Reference: https://tools.ietf.org/html/rfc7233#section-4.2
        """
        if self.end is None:
            content_range = "*"
        else:
            content_range = f"{self.start}-{self.end}"

        return f"{unit} {content_range}/{self.total}"


class SemanticErrorType(str, enum.Enum):
    """Enum with semantic error types"""
    ALREADY_DELETED = "semantic_error.already_deleted"
    INVALID_INPUT = "semantic_error.invalid_input"
    NOT_FOUND = "semantic_error.not_found"
    NOT_UNIQUE = "semantic_error.not_unique"


class SemanticError(Exception):
    """Exception raised for semantic errors"""

    def __init__(
        self,
        loc: dict,
        msg: Union[str, List[Dict[str, Any]]],
        type: SemanticErrorType,
    ):
        self.loc = loc
        self.msg = msg
        self.type = type
        super().__init__(self.msg)

    def detail(self):
        """Detail of this semantic error"""
        return {
            "loc": self.loc,
            "msg": self.msg,
            "type": self.type,
        }
