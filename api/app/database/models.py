"""This module contains the database models"""

from enum import Enum
import uuid
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base


class Status(str, Enum):
    """Status enum"""
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


class Form(Base):
    """The form table in the database"""

    __tablename__ = "form"
    default_sort_field = "name"

    id = sa.Column(
        pg.UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )
    name = sa.Column(sa.String(255), unique=True, index=True)
    slug = sa.Column(sa.String, unique=True, index=True)
    url = sa.Column(sa.String, nullable=True)
    status = sa.Column(sa.Enum(Status))
    form = sa.Column(sa.String)
    config = sa.Column(sa.String, nullable=True)
    organization_id = sa.Column(
        "organizationId", pg.UUID(as_uuid=True), nullable=True
    )
    organization_url = sa.Column("organizationUrl", sa.String, nullable=True)
    publish_date = sa.Column("publishDate", sa.Date, nullable=True)

    fulltext_fields = [{"modelName": "Form", "fields": ["name", "slug"]}]


class Css(Base):
    """The css table in the database"""

    __tablename__ = "css"
    default_sort_field = "name"

    id = sa.Column(
        pg.UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )
    name = sa.Column(sa.String(255), unique=True, index=True)
    primary_color = sa.Column("primaryColor", sa.String)
    secondary_color = sa.Column("secondaryColor", sa.String)
    font = sa.Column(sa.String)
    color_left = sa.Column("colorLeft", sa.String)
    background_color = sa.Column("backgroundColor", sa.String)
    logo = sa.Column(sa.String)

    fulltext_fields = [{"modelName": "Css", "fields": ["name"]}]
