"""This module contains the database session info"""

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from app.core import config

engine = create_engine(
    config.SQLALCHEMY_DATABASE_URI,
    future=True,
    echo=config.SQLALCHEMY_ECHO,
)

SessionLocal = sessionmaker(
    bind=engine,
    future=True,
    autocommit=False,
    autoflush=False,
)

Base = declarative_base()


def get_database():
    """Dependency for database"""

    database = SessionLocal()
    try:
        yield database
    finally:
        database.close()
