import * as React from "react";
import { render } from 'react-dom';
import { fetchUtils, Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { FormList, FormEdit, FormCreate, FormIcon } from './forms';


const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    options.headers.set('Access-Control-Expose-Headers', 'X-Total-Count')
    return fetchUtils.fetchJson(url, options);
};
const dataProvider = jsonServerProvider('http://localhost', httpClient);

const App = () => (
    <Admin dataProvider={dataProvider}>
        <Resource name="forms" list={FormList} edit={FormEdit} create={FormCreate} icon={FormIcon}/>
    </Admin>
);

export default App;

render(
    <Admin dataProvider={dataProvider} title="Example Admin">
        <Resource name="forms" list={FormList} edit={FormEdit} create={FormCreate} icon={FormIcon}/>
    </Admin>,
    document.getElementById('root')
);
