import * as React from "react";
import { List, Datagrid, Edit, Create, SimpleForm, TextField, EditButton, TextInput } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
export const FormIcon = BookIcon;

export const FormList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="slug" />
            <TextField source="name" />
            <TextField source="status" />
            <TextField source="form" />
            <TextField source="config" />
            <EditButton basePath="/forms" />
        </Datagrid>
    </List>
);

const FormTitle = ({ record }) => {
    return <span>Form {record ? `"${record.name}"` : ''}</span>;
};

export const FormEdit = (props) => (
    <Edit title={<FormTitle />} {...props}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <TextInput source="name" />
            <TextField source="slug" />
            <TextInput source="status" />
            <TextInput source="form" />
            <TextInput source="config" />
        </SimpleForm>
    </Edit>
);

export const FormCreate = (props) => (
    <Create title="Create a Form" {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="status" />
            <TextInput source="form" />
            <TextInput source="config" />
        </SimpleForm>
    </Create>
);
